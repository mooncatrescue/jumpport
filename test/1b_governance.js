require('dotenv').config()
const { network, ethers } = require('hardhat'),
  {
    JumpPort,
    expectPermissionError,
  } = require('@mooncatrescue/contracts/moonCatUtils'),
  gasLedger = require('../lib/gasLedger'),
  { expect } = require('chai')

const BLANK_BYTES32 = ethers.utils.hexZeroPad('0x00', 32)
//const CALL_SALT = ethers.utils.hexlify(ethers.utils.randomBytes(32));
const CALL_SALT =
  '0x034dc58f46f21d98834ab159a61709d956ddfd95bebccb9da821056794b32d52'

let currentSnapshot = false

let JUMPPORT = false
let DAO = false

const EIP712_TYPES = {
  Operation: [
    { name: 'target', type: 'address' },
    { name: 'payload', type: 'bytes' },
    { name: 'predecessor', type: 'bytes32' },
    { name: 'salt', type: 'bytes32' },
  ],
  BatchOperation: [
    { name: 'targets', type: 'address[]' },
    { name: 'payloads', type: 'bytes[]' },
    { name: 'predecessor', type: 'bytes32' },
    { name: 'salt', type: 'bytes32' },
  ],
}
let EIP721_DOMAIN = false

describe('TimelockController DAO basic logic', function () {
  before(async function () {
    const accounts = await ethers.getSigners()
    JUMPPORT = await JumpPort

    // Set first Hardhat account as an admin of the JumpPort
    const jumpportAdmin = '0xdf2E60Af57C411F848B1eA12B10a404d194bce27'
    await accounts[5].sendTransaction({
      to: jumpportAdmin,
      value: ethers.utils.parseEther('0.3'),
    })
    await network.provider.request({
      method: 'hardhat_impersonateAccount',
      params: [jumpportAdmin],
    })
    let adminSigner = await ethers.provider.getSigner(jumpportAdmin)
    await JUMPPORT.connect(adminSigner).setAdmin(accounts[0].address, true)

    const TimelockController = await ethers.getContractFactory(
      'TimelockController'
    )
    DAO = await TimelockController.deploy(100, 2, [
      accounts[5].address,
      accounts[6].address,
      accounts[7].address,
    ])
    //console.log('DAO Contract Deployed To:', DAO.address);
    EIP721_DOMAIN = {
      name: 'Timelock Governance',
      version: '1',
      chainId: network.config.chainId,
      verifyingContract: DAO.address,
    }

    // Make DAO an Admin of the JumpPort
    await JUMPPORT.setAdmin(DAO.address, true)
  })
  after(function () {
    gasLedger.reportGasUsage()
    gasLedger.exportLedger(__filename)
  })

  beforeEach(async function () {
    currentSnapshot = await network.provider.request({
      method: 'evm_snapshot',
      params: [],
    })
  })
  afterEach(async function () {
    await network.provider.request({
      method: 'evm_revert',
      params: [currentSnapshot],
    })
  })

  it('should allow DAO members to schedule calls', async function () {
    const accounts = await ethers.getSigners()
    const ADMIN1 = accounts[5]
    const ADMIN2 = accounts[6]
    const ADMIN_ROLE = await DAO.ADMIN_ROLE()
    const VOTER_ROLE = await DAO.VOTER_ROLE()
    const PORTAL_ROLE = await JUMPPORT.PORTAL_ROLE()

    expect(await DAO.hasRole(VOTER_ROLE, ADMIN1.address)).to.equal(
      true,
      'Admin 1 role set'
    )
    expect(await DAO.hasRole(VOTER_ROLE, ADMIN2.address)).to.equal(
      true,
      'Admin 2 role set'
    )
    expect(await DAO.hasRole(ADMIN_ROLE, DAO.address)).to.equal(
      true,
      'DAO role set'
    )
    expect(await DAO.hasRole(ADMIN_ROLE, ADMIN1.address)).to.equal(
      false,
      'Admin 1 not admin of DAO contract'
    )
    expect(await DAO.hasRole(ADMIN_ROLE, accounts[0].address)).to.equal(
      false,
      'Deployer not admin of DAO contract'
    )
    expect(await DAO.hasRole(VOTER_ROLE, accounts[0].address)).to.equal(
      false,
      'Deployer not voter of DAO contract'
    )

    // Build transaction parts
    let callData = (
      await DAO.populateTransaction.setVoter(accounts[2].address, true)
    ).data
    console.log('Call data', callData)
    let callHash = await DAO['hash((address,bytes,bytes32,bytes32))']([
      DAO.address,
      callData,
      BLANK_BYTES32,
      CALL_SALT,
    ])
    console.log('Call hash', callHash)

    // Non members should not be allowed to create scheduled calls
    await expectPermissionError(
      DAO.connect(accounts[2]).schedule(
        DAO.address,
        callData,
        BLANK_BYTES32,
        CALL_SALT,
        150
      ),
      VOTER_ROLE,
      accounts[2].address
    )

    expect(await DAO.isOperation(callHash)).to.equal(
      false,
      'Call does not exist yet'
    )
    let tx = await DAO.connect(ADMIN1).schedule(
      DAO.address,
      callData,
      BLANK_BYTES32,
      CALL_SALT,
      150
    )
    await gasLedger.gasUsed(tx, this.test.title, 'Schedule call')
    await expect(tx)
      .to.emit(DAO, 'CallScheduled')
      .withArgs(callHash, 0, DAO.address, callData, BLANK_BYTES32, 150)
    await expectCallState(callHash, true, true, false, false)

    // Try and execute calls before timeout
    await expect(
      DAO.connect(accounts[2]).execute(
        DAO.address,
        callData,
        BLANK_BYTES32,
        CALL_SALT
      )
    ).to.be.revertedWith('TimelockController: operation is not ready')
    await expect(
      DAO.connect(accounts[2]).execute(
        DAO.address,
        callData,
        BLANK_BYTES32,
        ethers.utils.randomBytes(32)
      )
    ).to.be.revertedWith(
      'TimelockController: operation is not ready',
      'Random operation is unknown'
    )

    // Jump forward the wait period
    await network.provider.send('hardhat_mine', [
      ethers.BigNumber.from('150').toHexString(),
    ])
    await expectCallState(callHash, true, true, true, false)

    await expect(
      DAO.connect(accounts[2]).execute(
        DAO.address,
        callData,
        BLANK_BYTES32,
        ethers.utils.randomBytes(32)
      )
    ).to.be.revertedWith(
      'TimelockController: operation is not ready',
      'Random operation is unknown'
    )
    tx = await DAO.connect(accounts[2]).execute(
      DAO.address,
      callData,
      BLANK_BYTES32,
      CALL_SALT
    )
    await gasLedger.gasUsed(tx, this.test.title, 'Execute call')
    await expect(tx)
      .to.emit(DAO, 'CallExecuted')
      .withArgs(callHash, 0, DAO.address, callData)
    await expect(tx)
      .to.emit(DAO, 'RoleChange')
      .withArgs(VOTER_ROLE, accounts[2].address, true, DAO.address)
    await expectCallState(callHash, true, false, false, true)

    await expect(
      DAO.connect(ADMIN1).schedule(
        DAO.address,
        callData,
        BLANK_BYTES32,
        CALL_SALT,
        500
      )
    ).to.be.revertedWith(
      'TimelockController: operation already scheduled',
      'Attempt to schedule identical call again'
    )

    // Craft a batch-call on the JumpPort
    let callDatas = []

    callData = (
      await JUMPPORT.populateTransaction.setPortalValidation(
        accounts[2].address,
        true
      )
    ).data
    callDatas.push(callData)

    callData = (
      await JUMPPORT.populateTransaction.setAdmin(accounts[3].address, true)
    ).data
    callDatas.push(callData)

    callData = (await DAO.populateTransaction.updateDelay(500)).data
    callDatas.push(callData)

    callData = (await JUMPPORT.populateTransaction.setPaused(false)).data
    callDatas.push(callData)

    let callAddresses = [
      JUMPPORT.address,
      JUMPPORT.address,
      DAO.address,
      JUMPPORT.address,
    ]

    console.log('Batch calls', callDatas)

    let batchId = await DAO['hash((address[],bytes[],bytes32,bytes32))']([
      callAddresses,
      callDatas,
      BLANK_BYTES32,
      CALL_SALT,
    ])
    console.log('Batch schedule ID', batchId)

    tx = await DAO.connect(ADMIN1).scheduleBatch(
      callAddresses,
      callDatas,
      BLANK_BYTES32,
      CALL_SALT,
      150
    )
    await gasLedger.gasUsed(tx, this.test.title, 'Schedule batch call')
    await expect(tx)
      .to.emit(DAO, 'CallScheduled')
      .withArgs(batchId, 2, callAddresses[2], callDatas[2], BLANK_BYTES32, 150)

    // Jump forward the wait period
    await network.provider.send('hardhat_mine', [
      ethers.BigNumber.from('150').toHexString(),
    ])

    tx = await DAO.connect(accounts[2]).executeBatch(
      callAddresses,
      callDatas,
      BLANK_BYTES32,
      CALL_SALT
    )
    await gasLedger.gasUsed(tx, this.test.title, 'Execute batch call')
    await expect(tx)
      .to.emit(DAO, 'CallExecuted')
      .withArgs(batchId, 2, callAddresses[2], callDatas[2])
    await expect(tx)
      .to.emit(JUMPPORT, 'RoleChange')
      .withArgs(PORTAL_ROLE, accounts[2].address, true, DAO.address)
    await expect(tx).to.emit(DAO, 'MinDelayChange').withArgs(100, 500)
    expect(await JUMPPORT.depositPaused()).to.equal(false)

    await expect(
      DAO.connect(accounts[2]).executeBatch(
        callAddresses,
        callDatas,
        BLANK_BYTES32,
        CALL_SALT
      )
    ).to.be.revertedWith(
      'TimelockController: operation is not ready',
      'Attempt to execute again after successful call'
    )

    await expectPermissionError(
      DAO.connect(accounts[2]).updateDelay(600),
      ADMIN_ROLE,
      accounts[2].address
    )
  })

  it('should allow cancelling operations', async function () {
    const accounts = await ethers.getSigners()
    const ADMIN1 = accounts[5]
    const ADMIN2 = accounts[6]
    const ADMIN3 = accounts[7]

    // Build transaction parts
    let callData = (await DAO.populateTransaction.updateQuorum(42)).data
    let callHash = await DAO['hash((address,bytes,bytes32,bytes32))']([
      DAO.address,
      callData,
      BLANK_BYTES32,
      CALL_SALT,
    ])

    await expect(DAO.connect(ADMIN1).cancel(callHash)).to.be.revertedWith(
      'TimelockController: operation cannot be cancelled',
      'Attempt to cancel before scheduling'
    )

    // Schedule call with Admin #1
    await scheduleCall({
      signer: ADMIN1,
      targetContract: DAO.address,
      callData,
    })

    // Cancel call with Admin #2
    let tx = await DAO.connect(ADMIN2).cancel(callHash)
    await gasLedger.gasUsed(tx, this.test.title, 'Cancel call')
    await expect(tx).to.emit(DAO, 'Cancelled').withArgs(callHash)
    await expectCallState(callHash, false, false, false, false)

    // Schedule call again, with Admin #3
    // Allowed, because original call was cancelled
    await scheduleCall({
      signer: ADMIN3,
      targetContract: DAO.address,
      callData,
    })

    // Jump forward the wait period
    await network.provider.send('hardhat_mine', [
      ethers.BigNumber.from('150').toHexString(),
    ])
    await expectCallState(callHash, true, true, true, false)
    await DAO.connect(accounts[2]).execute(
      DAO.address,
      callData,
      BLANK_BYTES32,
      CALL_SALT
    )
    await expectCallState(callHash, true, false, false, true)

    await expect(DAO.connect(ADMIN1).cancel(callHash)).to.be.revertedWith(
      'TimelockController: operation cannot be cancelled',
      'Attempt to cancel after execution'
    )
  })

  it('should allow executing calls early, if quorum is met', async function () {
    const accounts = await ethers.getSigners()
    const ADMIN1 = accounts[5]
    const ADMIN2 = accounts[6]
    const ADMIN3 = accounts[7]

    // Build transaction parts
    let callData = (await DAO.populateTransaction.updateQuorum(42)).data

    // Schedule call with Admin #1
    let callHash = await scheduleCall({
      signer: ADMIN1,
      targetContract: DAO.address,
      callData,
    })

    // Random address cannot execute
    await expectSignatureError(
      DAO.connect(accounts[1]).executeEarly(
        DAO.address,
        callData,
        BLANK_BYTES32,
        CALL_SALT,
        []
      ),
      2,
      0
    )
    // Requester cannot execute without quorum
    await expectSignatureError(
      DAO.connect(ADMIN1).executeEarly(
        DAO.address,
        callData,
        BLANK_BYTES32,
        CALL_SALT,
        []
      ),
      2,
      1
    )
    // Submitter counts as one signer, but quorum still not met
    await expectSignatureError(
      DAO.connect(ADMIN2).executeEarly(
        DAO.address,
        callData,
        BLANK_BYTES32,
        CALL_SALT,
        []
      ),
      2,
      1
    )

    // Create EIP712 message for Admin #3 to sign
    let message = {
      target: DAO.address,
      payload: callData,
      predecessor: BLANK_BYTES32,
      salt: CALL_SALT,
    }
    let types = { Operation: EIP712_TYPES.Operation }
    console.log(
      JSON.stringify(
        ethers.utils._TypedDataEncoder.getPayload(
          EIP721_DOMAIN,
          types,
          message
        ),
        null,
        2
      )
    )

    // Verify Domain Separator
    let domainSeparator =
      ethers.utils._TypedDataEncoder.hashDomain(EIP721_DOMAIN)
    console.log('Domain separator', domainSeparator)
    expect(await DAO.DOMAIN_SEPARATOR()).to.equal(
      domainSeparator,
      'Domain Separator'
    )

    // Verify message hash
    let messageHash = ethers.utils._TypedDataEncoder.hash(
      EIP721_DOMAIN,
      types,
      message
    )
    expect(
      await DAO['getProxyMessage((address,bytes,bytes32,bytes32))'](message)
    ).to.equal(messageHash, 'Message payload')

    // Create signature for Admin #3
    // https://docs.ethers.io/v5/api/utils/hashing/#TypedDataEncoder-hash
    let signatures = [
      await ADMIN3._signTypedData(EIP721_DOMAIN, types, message),
    ]
    expect(await DAO.validSignature(messageHash, signatures[0])).to.equal(
      true,
      'Signature from Admin #3'
    )

    let tx = await DAO.connect(ADMIN1).executeEarly(
      DAO.address,
      callData,
      BLANK_BYTES32,
      CALL_SALT,
      signatures
    )
    await gasLedger.gasUsed(tx, this.test.title, 'Execute call')
    await expect(tx)
      .to.emit(DAO, 'CallExecuted')
      .withArgs(callHash, 0, DAO.address, callData)
    await expect(tx).to.emit(DAO, 'ExecuteEarlyQuorumChange').withArgs(2, 42)
    await expectCallState(callHash, true, false, false, true)
    expect(await DAO.executeEarlyQuorum()).to.equal(42, 'Quorum amount updated')

    await expect(
      DAO.connect(ADMIN1).executeEarly(
        DAO.address,
        callData,
        BLANK_BYTES32,
        CALL_SALT,
        signatures
      )
    ).to.be.revertedWith(
      'TimelockController: operation is not pending',
      'Attempt to execute again after successful call'
    )

    const ADMIN_ROLE = await DAO.ADMIN_ROLE()
    await expectPermissionError(
      DAO.connect(ADMIN1).updateQuorum(15),
      ADMIN_ROLE,
      ADMIN1.address
    )
  })

  it('should allow executing batch calls early, if quorum is met', async function () {
    const accounts = await ethers.getSigners()
    const ADMIN1 = accounts[5]
    const ADMIN2 = accounts[6]
    const ADMIN3 = accounts[7]

    // Craft a batch-call
    let callDatas = []

    let callData = (await DAO.populateTransaction.updateDelay(500)).data
    callDatas.push(callData)

    callData = (await DAO.populateTransaction.updateQuorum(42)).data
    callDatas.push(callData)

    let callAddresses = [DAO.address, DAO.address]
    let batchId = await DAO['hash((address[],bytes[],bytes32,bytes32))']([
      callAddresses,
      callDatas,
      BLANK_BYTES32,
      CALL_SALT,
    ])

    // Schedule call with Admin #1
    await DAO.connect(ADMIN1).scheduleBatch(
      callAddresses,
      callDatas,
      BLANK_BYTES32,
      CALL_SALT,
      150
    )
    await expectCallState(batchId, true, true, false, false)

    // Random address cannot execute
    await expectSignatureError(
      DAO.connect(accounts[1]).executeBatchEarly(
        callAddresses,
        callDatas,
        BLANK_BYTES32,
        CALL_SALT,
        []
      ),
      2,
      0
    )
    // Requester cannot execute without quorum
    await expectSignatureError(
      DAO.connect(ADMIN1).executeBatchEarly(
        callAddresses,
        callDatas,
        BLANK_BYTES32,
        CALL_SALT,
        []
      ),
      2,
      1
    )
    // Submitter counts as one signer, but quorum still not met
    await expectSignatureError(
      DAO.connect(ADMIN2).executeBatchEarly(
        callAddresses,
        callDatas,
        BLANK_BYTES32,
        CALL_SALT,
        []
      ),
      2,
      1
    )

    // Create EIP712 message for Admin #3 to sign
    let message = {
      targets: callAddresses,
      payloads: callDatas,
      predecessor: BLANK_BYTES32,
      salt: CALL_SALT,
    }
    let types = { BatchOperation: EIP712_TYPES.BatchOperation }
    console.log(
      JSON.stringify(
        ethers.utils._TypedDataEncoder.getPayload(
          EIP721_DOMAIN,
          types,
          message
        ),
        null,
        2
      )
    )

    //console.log(encodeType('Operation', EIP712_TYPES));
    //console.log(encodeType('BatchOperation', EIP712_TYPES));

    // Compare the typeHash values for this contract's custom data types to the standard's computations
    let batchHash = typeHash('BatchOperation', EIP712_TYPES)
    console.log('BatchOperation typeHash', batchHash)
    expect(await DAO.BATCHOPERATION_TYPEHASH()).to.equal(
      batchHash,
      'BatchOperation typeHash'
    )

    // Verify message hash
    let messageHash = ethers.utils._TypedDataEncoder.hash(
      EIP721_DOMAIN,
      types,
      message
    )
    expect(
      await DAO['getProxyMessage((address[],bytes[],bytes32,bytes32))'](message)
    ).to.equal(messageHash, 'Message payload')

    // Create signature for Admin #3
    // https://docs.ethers.io/v5/api/utils/hashing/#TypedDataEncoder-hash
    let signatures = [
      await ADMIN3._signTypedData(EIP721_DOMAIN, types, message),
    ]
    expect(await DAO.validSignature(messageHash, signatures[0])).to.equal(
      true,
      'Signature from Admin #3'
    )

    let tx = await DAO.connect(ADMIN1).executeBatchEarly(
      callAddresses,
      callDatas,
      BLANK_BYTES32,
      CALL_SALT,
      signatures
    )
    await gasLedger.gasUsed(tx, this.test.title, 'Execute call')
    await expect(tx)
      .to.emit(DAO, 'CallExecuted')
      .withArgs(batchId, 1, callAddresses[1], callDatas[1])
    await expect(tx).to.emit(DAO, 'ExecuteEarlyQuorumChange').withArgs(2, 42)
    await expectCallState(batchId, true, false, false, true)
    expect(await DAO.executeEarlyQuorum()).to.equal(42, 'Quorum amount updated')

    await expect(
      DAO.connect(ADMIN1).executeBatchEarly(
        callAddresses,
        callDatas,
        BLANK_BYTES32,
        CALL_SALT,
        signatures
      )
    ).to.be.revertedWith(
      'TimelockController: operation is not pending',
      'Attempt to execute again after successful call'
    )
  })

  it('should bubble-up errors from called contracts', async function () {
    const accounts = await ethers.getSigners()
    const ADMIN1 = accounts[5]
    const ADMIN2 = accounts[6]
    const ADMIN3 = accounts[7]

    // Prepare a transaction that will fail
    let callData = (
      await JUMPPORT.populateTransaction['deposit(address[],uint256[])'](
        [ADMIN1.address, ADMIN2.address],
        [100]
      )
    ).data

    // Schedule call with Admin #1
    let callHash = await scheduleCall({
      signer: ADMIN1,
      targetContract: JUMPPORT.address,
      callData,
    })

    // Jump forward the wait period
    await network.provider.send('hardhat_mine', [
      ethers.BigNumber.from('150').toHexString(),
    ])
    await expectCallState(callHash, true, true, true, false)

    await expect(
      DAO.connect(accounts[2]).execute(
        JUMPPORT.address,
        callData,
        BLANK_BYTES32,
        CALL_SALT
      )
    ).to.be.revertedWith(
      'Mismatched inputs',
      'Transaction fails with custom error from sub-contract'
    )
  })

  it('should resist attacks by bad actors', async function () {
    const accounts = await ethers.getSigners()
    const ALICE = accounts[5]
    const BOB = accounts[6]
    const EVE = accounts[7]

    // Eve goes rogue and creates a bunch of calls beneficial to her
    let maliciousCalls = []
    maliciousCalls.push(
      await scheduleCall({
        signer: EVE,
        targetContract: DAO.address,
        callData: (
          await DAO.populateTransaction.setAdmin(accounts[1].address, true)
        ).data,
      })
    )
    maliciousCalls.push(
      await scheduleCall({
        signer: EVE,
        targetContract: DAO.address,
        callData: (
          await DAO.populateTransaction.setAdmin(accounts[2].address, true)
        ).data,
      })
    )
    maliciousCalls.push(
      await scheduleCall({
        signer: EVE,
        targetContract: DAO.address,
        callData: (
          await DAO.populateTransaction.setAdmin(accounts[3].address, true)
        ).data,
      })
    )
    maliciousCalls.push(
      await scheduleCall({
        signer: EVE,
        targetContract: DAO.address,
        callData: (
          await DAO.populateTransaction.setAdmin(EVE.address, true)
        ).data,
      })
    )
    maliciousCalls.push(
      await scheduleCall({
        signer: EVE,
        targetContract: DAO.address,
        callData: (
          await DAO.populateTransaction.setVoter(ALICE.address, false)
        ).data,
      })
    )
    maliciousCalls.push(
      await scheduleCall({
        signer: EVE,
        targetContract: DAO.address,
        callData: (
          await DAO.populateTransaction.setVoter(BOB.address, false)
        ).data,
      })
    )

    // Alice mass-cancels them
    let tx = await DAO.connect(ALICE).cancelMultiple(maliciousCalls)
    await gasLedger.gasUsed(
      tx,
      this.test.title,
      `Cancel ${maliciousCalls.length} calls at once`
    )
    await expect(tx)
      .to.emit(DAO, 'Cancelled')
      .withArgs(maliciousCalls[maliciousCalls.length - 1])

    // Alice and Bob collude to oust Eve:
    const VOTER_ROLE = await DAO.VOTER_ROLE()

    // Alice crafts transaction to remove Eve's voting rights
    let callData = (await DAO.populateTransaction.setVoter(EVE.address, false))
      .data
    let callHash = await DAO['hash((address,bytes,bytes32,bytes32))']([
      DAO.address,
      callData,
      BLANK_BYTES32,
      CALL_SALT,
    ])

    // Create signature for Bob
    let message = {
      target: DAO.address,
      payload: callData,
      predecessor: BLANK_BYTES32,
      salt: CALL_SALT,
    }
    let types = { Operation: EIP712_TYPES.Operation }
    let signatures = [await BOB._signTypedData(EIP721_DOMAIN, types, message)]
    let messageHash = ethers.utils._TypedDataEncoder.hash(
      EIP721_DOMAIN,
      types,
      message
    )
    expect(await DAO.validSignature(messageHash, signatures[0])).to.equal(
      true,
      'Signature from Bob'
    )

    // Alice schedules and executes the call in one action, preventing Eve from having the chance to cancel
    tx = await DAO.connect(ALICE).scheduleAndExecute(
      DAO.address,
      callData,
      BLANK_BYTES32,
      CALL_SALT,
      signatures
    )
    await gasLedger.gasUsed(tx, this.test.title, 'Schedule-and-execute call')
    await expect(tx)
      .to.emit(DAO, 'CallScheduled')
      .withArgs(callHash, 0, DAO.address, callData, BLANK_BYTES32, 100)
    await expect(tx)
      .to.emit(DAO, 'CallExecuted')
      .withArgs(callHash, 0, DAO.address, callData)
    await expect(tx)
      .to.emit(DAO, 'RoleChange')
      .withArgs(VOTER_ROLE, EVE.address, false, DAO.address)

    // Alice and Bob realize other addresses are related to the scam too
    const PORTAL_ROLE = await JUMPPORT.PORTAL_ROLE()

    // Craft a batch-call on the JumpPort
    let callDatas = []

    callData = (
      await JUMPPORT.populateTransaction.setPortalValidation(
        accounts[1].address,
        false
      )
    ).data
    callDatas.push(callData)
    callData = (
      await JUMPPORT.populateTransaction.setPortalValidation(
        accounts[2].address,
        false
      )
    ).data
    callDatas.push(callData)
    callData = (
      await JUMPPORT.populateTransaction.setPortalValidation(
        accounts[3].address,
        false
      )
    ).data
    callDatas.push(callData)

    let callAddresses = [JUMPPORT.address, JUMPPORT.address, JUMPPORT.address]

    let batchId = await DAO['hash((address[],bytes[],bytes32,bytes32))']([
      callAddresses,
      callDatas,
      BLANK_BYTES32,
      CALL_SALT,
    ])
    message = {
      targets: callAddresses,
      payloads: callDatas,
      predecessor: BLANK_BYTES32,
      salt: CALL_SALT,
    }
    types = { BatchOperation: EIP712_TYPES.BatchOperation }
    signatures = [await BOB._signTypedData(EIP721_DOMAIN, types, message)]

    tx = await DAO.connect(ALICE).scheduleAndExecuteBatch(
      callAddresses,
      callDatas,
      BLANK_BYTES32,
      CALL_SALT,
      signatures
    )
    await gasLedger.gasUsed(
      tx,
      this.test.title,
      'Schedule-and-execute batch call'
    )
    await expect(tx)
      .to.emit(DAO, 'CallScheduled')
      .withArgs(batchId, 2, callAddresses[2], callDatas[2], BLANK_BYTES32, 100)
    await expect(tx)
      .to.emit(DAO, 'CallExecuted')
      .withArgs(batchId, 2, callAddresses[2], callDatas[2])
    await expect(tx)
      .to.emit(JUMPPORT, 'RoleChange')
      .withArgs(PORTAL_ROLE, accounts[2].address, false, DAO.address)
  })
})

async function expectCallState(callId, doesExist, isPending, isReady, isDone) {
  expect(await DAO.isOperation(callId)).to.equal(doesExist, 'Call exists')
  expect(await DAO.isOperationPending(callId)).to.equal(
    isPending,
    'Call pending'
  )
  expect(await DAO.isOperationReady(callId)).to.equal(isReady, 'Call ready')
  expect(await DAO.isOperationDone(callId)).to.equal(isDone, 'Call done')
}

async function scheduleCall({
  signer,
  targetContract = DAO.address,
  callData,
  predecessor = BLANK_BYTES32,
  salt = CALL_SALT,
  delay = 150,
}) {
  let callHash = await DAO['hash((address,bytes,bytes32,bytes32))']([
    targetContract,
    callData,
    predecessor,
    salt,
  ])
  await DAO.connect(signer).schedule(
    targetContract,
    callData,
    predecessor,
    salt,
    delay
  )
  await expectCallState(callHash, true, true, false, false)
  return callHash
}

async function expectSignatureError(tx, minRequired, amountSupplied) {
  try {
    await tx
    expect.fail('Expected transaction to revert, but it succeeded')
  } catch (err) {
    let expectedMessage = `VM Exception while processing transaction: reverted with custom error 'InsufficientSignatures(${minRequired}, ${amountSupplied})'`
    expect(err.message).to.equal(
      expectedMessage,
      'Insufficient Signatures error thrown'
    )
  }
}

async function makeSignature(signer, callId) {
  let rawMessage = ethers.utils.solidityKeccak256(
    ['string', 'address', 'bytes32'],
    ['approve', DAO.address, callId]
  )
  //console.log(rawMessage);
  let signature = await signer.signMessage(ethers.utils.arrayify(rawMessage))
  //console.log(message);
  return signature
}

// EIP712 manual signing
// https://github.com/ethereum/EIPs/blob/master/assets/eip-712/Example.js

function dependencies(types, primaryType, found = []) {
  if (found.includes(primaryType)) {
    return found
  }
  if (types[primaryType] === undefined) {
    return found
  }
  found.push(primaryType)
  for (let field of types[primaryType]) {
    for (let dep of dependencies(types, field.type, found)) {
      if (!found.includes(dep)) {
        found.push(dep)
      }
    }
  }
  return found
}

function encodeType(primaryType, types) {
  // Get dependencies primary first, then alphabetical
  let deps = dependencies(types, primaryType)
  deps = deps.filter((t) => t != primaryType)
  deps = [primaryType].concat(deps.sort())

  // Format as a string with fields
  let result = ''
  for (let type of deps) {
    result += `${type}(${types[type]
      .map(({ name, type }) => `${type} ${name}`)
      .join(',')})`
  }
  return result
}

function typeHash(primaryType, types) {
  return ethers.utils.keccak256(
    ethers.utils.toUtf8Bytes(encodeType(primaryType, types))
  )
}

function encodeData(primaryType, types, data) {
  let encTypes = []
  let encValues = []

  // Add typehash
  encTypes.push('bytes32')
  encValues.push(typeHash(primaryType, types))

  // Add field contents
  for (let field of types[primaryType]) {
    let value = data[field.name]
    if (field.type == 'string' || field.type == 'bytes') {
      encTypes.push('bytes32')
      value = ethers.utils.keccak256(ethers.utils.toUtf8Bytes(value))
      encValues.push(value)
    } else if (types[field.type] !== undefined) {
      encTypes.push('bytes32')
      value = ethers.utils.keccak256(encodeData(field.type, types, value))
      encValues.push(value)
    } else if (field.type.lastIndexOf(']') === field.type.length - 1) {
      throw 'TODO: Arrays currently unimplemented in encodeData'
    } else {
      encTypes.push(field.type)
      encValues.push(value)
    }
  }

  return abiCoder.encode(encTypes, encValues)
}

function structHash(primaryType, types, data) {
  return ethers.utils.keccak256(encodeData(primaryType, types, data))
}

function signHash(typedData) {
  return ethers.utils.keccak256(
    Buffer.concat([
      Buffer.from('1901', 'hex'),
      Buffer.from(
        structHash('EIP712Domain', typedData.types, typedData.domain).substr(2),
        'hex'
      ),
      Buffer.from(
        structHash(
          typedData.primaryType,
          typedData.types,
          typedData.message
        ).substr(2),
        'hex'
      ),
    ])
  )
}
