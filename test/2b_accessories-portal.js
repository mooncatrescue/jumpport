require('dotenv').config()
const { network, ethers } = require('hardhat'),
  gasLedger = require('../lib/gasLedger'),
  {
    MoonCatAcclimator,
    MoonCatAccessories,
    JumpPort,
    disperseMoonCats,
    expectPermissionError,
  } = require('@mooncatrescue/contracts/moonCatUtils'),
  { expect } = require('chai')

let currentSnapshot = false
let moonCatOwners = {}
const abiCoder = ethers.utils.defaultAbiCoder

let ACCLIMATOR = false
let ACCESSORIES = false
let JUMPPORT = false
let PORTAL = false
let REFERRER = false

const EIP712_TYPES = {
  AccessoryBatchData: [
    { name: 'rescueOrder', type: 'uint256' },
    { name: 'accessory', type: 'uint232' },
    { name: 'paletteIndex', type: 'uint8' },
    { name: 'zIndex', type: 'uint16' },
  ],
  AccessoryPurchaseRequest: [
    { name: 'expirationTimestamp', type: 'uint256' },
    { name: 'nonce', type: 'uint256' },
    { name: 'purchaseData', type: 'AccessoryBatchData' },
    { name: 'tributeToken', type: 'address' },
    { name: 'tributeAmount', type: 'uint256' },
  ],
}
let EIP721_DOMAIN = false

const DAI = new ethers.Contract('0x6b175474e89094c44da98b954eedeac495271d0f', [
  'function approve(address spender, uint256 amount) external returns (bool)',
  'function balanceOf(address account) external view returns (uint256)',
  'function transfer(address recipient, uint256 amount) external returns (bool)',
  'function transferFrom(address sender, address recipient, uint256 amount) external returns (bool)',
])
let DAI_HOLDER = false

describe('MoonCat Accessories Portal basic functionality', function () {
  before(async function () {
    const accounts = await ethers.getSigners()
    REFERRER = accounts[5]
    ACCLIMATOR = await MoonCatAcclimator
    ACCESSORIES = await MoonCatAccessories
    JUMPPORT = await JumpPort

    // Set first Hardhat account as an admin of the JumpPort
    const jumpportAdmin = '0xdf2E60Af57C411F848B1eA12B10a404d194bce27'
    await accounts[5].sendTransaction({
      to: jumpportAdmin,
      value: ethers.utils.parseEther('0.3'),
    })
    await network.provider.request({
      method: 'hardhat_impersonateAccount',
      params: [jumpportAdmin],
    })
    let adminSigner = await ethers.provider.getSigner(jumpportAdmin)
    await JUMPPORT.connect(adminSigner).setAdmin(accounts[0].address, true)

    let daiHolderAddress = '0x8EB8a3b98659Cce290402893d0123abb75E3ab28'
    DAI_HOLDER = await ethers.provider.getSigner(daiHolderAddress)
    await hre.network.provider.request({
      method: 'hardhat_impersonateAccount',
      params: [daiHolderAddress],
    })

    if (network.name == 'hardhat') {
      // Set up test accounts as the owners of several cats
      this.timeout(300000)
      await disperseMoonCats(1000, 1020)
    }
    moonCatOwners = await updateMoonCatOwners(1000, 1020)

    const AccessoriesPortal = await ethers.getContractFactory(
      'AccessoriesPortal'
    )
    PORTAL = await AccessoriesPortal.deploy(
      JUMPPORT.address,
      REFERRER.address,
      ACCLIMATOR.address,
      ACCESSORIES.address
    )
    //console.log('AccessoriesPortal Contract Deployed To:', PORTAL.address);

    await JUMPPORT.setPortalValidation(PORTAL.address, true)
    EIP721_DOMAIN = {
      name: 'Accessories Portal',
      version: '1',
      chainId: network.config.chainId,
      verifyingContract: PORTAL.address,
    }
  })
  after(function () {
    gasLedger.reportGasUsage()
    gasLedger.exportLedger(__filename)
  })

  beforeEach(async function () {
    currentSnapshot = await network.provider.request({
      method: 'evm_snapshot',
      params: [],
    })
  })
  afterEach(async function () {
    await network.provider.request({
      method: 'evm_revert',
      params: [currentSnapshot],
    })
  })

  it('should allow buying accessories for JumpPort MoonCats', async function () {
    const { Alice, Bob, Eve } = await setup(),
      KEY_MOONCAT = moonCatOwners[Alice.address][0],
      KEY_ACCESSORY = 21 // Boutique Shades

    // MoonCat should start as not owning accessories
    expect(
      await ACCESSORIES.doesMoonCatOwnAccessory(KEY_MOONCAT, KEY_ACCESSORY)
    ).to.equal(false, 'Initially does not own')

    const { price } = await ACCESSORIES.accessoryInfo(KEY_ACCESSORY)
    await expect(
      PORTAL.connect(Eve).buyAccessory(KEY_MOONCAT, KEY_ACCESSORY, 0, 100, {
        value: price,
      })
    ).to.be.revertedWith(
      'Buy Accessory caller is not owner nor approved',
      'Try to buy as non-owner'
    )

    await expect(
      PORTAL.connect(Alice).buyAccessory(KEY_MOONCAT, KEY_ACCESSORY, 0, 100, {
        value: 2,
      })
    ).to.be.revertedWith(
      'Insufficient Value',
      'Error from actual Accessories contract should bubble up'
    )

    let payload = (
      await ACCESSORIES.populateTransaction[
        'buyAccessory(uint256,uint256,uint8,uint16,address)'
      ](KEY_MOONCAT, KEY_ACCESSORY, 0, 100, REFERRER.address)
    ).data
    let tx = await PORTAL.connect(Alice).buyAccessory(
      KEY_MOONCAT,
      KEY_ACCESSORY,
      0,
      100,
      { value: price }
    )
    await gasLedger.gasUsed(tx, this.test.title, 'Buy single accessory')
    await expect(tx)
      .to.emit(JUMPPORT, 'ActionExecuted')
      .withArgs(ACCLIMATOR.address, KEY_MOONCAT, ACCESSORIES.address, payload)
    await expect(tx)
      .to.emit(ACCESSORIES, 'AccessoryPurchased')
      .withArgs(KEY_ACCESSORY, KEY_MOONCAT, price)

    expect(
      await ACCESSORIES.doesMoonCatOwnAccessory(KEY_MOONCAT, KEY_ACCESSORY)
    ).to.equal(true, 'Now owns')

    // Compare gas cost to just interacting directly
    tx = await ACCESSORIES.connect(Bob)[
      'buyAccessory(uint256,uint256,uint8,uint16,address)'
    ](moonCatOwners[Bob.address][0], KEY_ACCESSORY, 0, 100, REFERRER.address, {
      value: price,
    })
    await gasLedger.gasUsed(
      tx,
      this.test.title,
      'Buy single accessory directly from Accessories contract'
    )
  })

  it('should allow JumpPort Administrators to prevent execution', async function () {
    const { Alice, Deb, Eve } = await setup(),
      KEY_MOONCAT = moonCatOwners[Alice.address][0],
      KEY_ACCESSORY = 21, // Boutique Shades
      ADMIN_ROLE = await PORTAL.ADMIN_ROLE(),
      { price } = await ACCESSORIES.accessoryInfo(KEY_ACCESSORY)

    expect(await JUMPPORT.executionBlocked(PORTAL.address)).to.equal(
      false,
      'Execution starts unblocked'
    )

    await expectPermissionError(
      JUMPPORT.connect(Eve).setAdminExecutionBlocked(PORTAL.address, true),
      ADMIN_ROLE,
      Eve.address
    )

    await JUMPPORT.connect(Deb).setAdminExecutionBlocked(PORTAL.address, true)
    expect(await JUMPPORT.executionBlocked(PORTAL.address)).to.equal(
      true,
      'Execution now blocked'
    )
    await expect(
      PORTAL.connect(Alice).buyAccessory(KEY_MOONCAT, KEY_ACCESSORY, 0, 100, {
        value: price,
      })
    ).to.be.revertedWith(
      'Execution blocked for that Portal',
      'Try to execute from blocked Portal'
    )

    await JUMPPORT.connect(Deb).setAdminExecutionBlocked(PORTAL.address, false)
    expect(await JUMPPORT.executionBlocked(PORTAL.address)).to.equal(
      false,
      'Execution now unblocked'
    )
    await PORTAL.connect(Alice).buyAccessory(
      KEY_MOONCAT,
      KEY_ACCESSORY,
      0,
      100,
      { value: price }
    )
  })

  it('should allow Portal Administrators to prevent execution', async function () {
    const { Alice, Deb, Eve } = await setup(),
      KEY_MOONCAT = moonCatOwners[Alice.address][0],
      KEY_ACCESSORY = 21, // Boutique Shades
      ADMIN_ROLE = await JUMPPORT.ADMIN_ROLE(),
      PORTAL_ROLE = await JUMPPORT.PORTAL_ROLE(),
      { price } = await ACCESSORIES.accessoryInfo(KEY_ACCESSORY)

    expect(await JUMPPORT.executionBlocked(PORTAL.address)).to.equal(
      false,
      'Execution starts unblocked'
    )

    await expectPermissionError(
      JUMPPORT.connect(Eve).blockExecution(true),
      PORTAL_ROLE,
      Eve.address
    )

    await expectPermissionError(
      PORTAL.connect(Eve).blockExecution(true),
      ADMIN_ROLE,
      Eve.address
    )

    await PORTAL.connect(Deb).blockExecution(true)
    expect(await JUMPPORT.executionBlocked(PORTAL.address)).to.equal(
      true,
      'Execution now blocked'
    )
    await expect(
      PORTAL.connect(Alice).buyAccessory(KEY_MOONCAT, KEY_ACCESSORY, 0, 100, {
        value: price,
      })
    ).to.be.revertedWith(
      'Execution blocked for that Portal',
      'Try to execute from blocked Portal'
    )

    await PORTAL.connect(Deb).blockExecution(false)
    expect(await JUMPPORT.executionBlocked(PORTAL.address)).to.equal(
      false,
      'Execution now unblocked'
    )
    await PORTAL.connect(Alice).buyAccessory(
      KEY_MOONCAT,
      KEY_ACCESSORY,
      0,
      100,
      { value: price }
    )
  })

  it('should allow purchasing multiple accessories at once', async function () {
    const { Alice, Eve } = await setup(),
      KEY_MOONCAT = moonCatOwners[Alice.address][0]

    let prices = [
      (await ACCESSORIES.accessoryInfo(20)).price,
      (await ACCESSORIES.accessoryInfo(21)).price,
    ]

    // Assemble expected payload
    let orders = [
      [KEY_MOONCAT, 20, 0, 10],
      [KEY_MOONCAT, 21, 0, 10],
    ]
    let payload = (
      await ACCESSORIES.populateTransaction[
        'buyAccessories((uint256,uint232,uint8,uint16)[],address)'
      ](orders, REFERRER.address)
    ).data

    await expect(
      PORTAL.connect(Eve).buyAccessories(orders, {
        value: prices[0].add(prices[1]),
      })
    ).to.be.revertedWith(
      'Buy Accessories caller is not owner nor approved',
      'Attempt to purchase as non-owner'
    )

    let tmpOrders = JSON.parse(JSON.stringify(orders))
    tmpOrders[1][0] = moonCatOwners[Alice.address][1]
    await expect(
      PORTAL.connect(Alice).buyAccessories(tmpOrders, {
        value: prices[0].add(prices[1]),
      })
    ).to.be.revertedWith(
      'Rescue orders not all the same',
      'Attempt to purchase for different MoonCats at once'
    )

    let tx = await PORTAL.connect(Alice).buyAccessories(orders, {
      value: prices[0].add(prices[1]),
    })
    await gasLedger.gasUsed(tx, this.test.title, 'Buy two accessories at once')
    await expect(tx)
      .to.emit(JUMPPORT, 'ActionExecuted')
      .withArgs(ACCLIMATOR.address, KEY_MOONCAT, ACCESSORIES.address, payload)
    await expect(tx)
      .to.emit(ACCESSORIES, 'AccessoryPurchased')
      .withArgs(20, KEY_MOONCAT, prices[0])
    await expect(tx)
      .to.emit(ACCESSORIES, 'AccessoryPurchased')
      .withArgs(21, KEY_MOONCAT, prices[1])
  })

  it('should allow designating an approved operator for a token', async function () {
    const { Alice, Bob, Eve } = await setup(),
      KEY_MOONCAT = moonCatOwners[Alice.address][0],
      KEY_ACCESSORY = 21, // Boutique Shades
      { price } = await ACCESSORIES.accessoryInfo(KEY_ACCESSORY)

    expect(await PORTAL.getApproved(KEY_MOONCAT)).to.equal(
      ethers.constants.AddressZero,
      'Tokens start unapproved'
    )

    await expect(
      PORTAL.connect(Bob).buyAccessory(KEY_MOONCAT, KEY_ACCESSORY, 0, 100, {
        value: price,
      })
    ).to.be.revertedWith(
      'Buy Accessory caller is not owner nor approved',
      "Try to buy an Accessory as Bob, for Alice's MoonCat"
    )

    await expect(
      PORTAL.connect(Eve).approve(Eve.address, KEY_MOONCAT)
    ).to.be.revertedWith(
      'Approve caller is not owner nor approved',
      "Eve attempt to approve herself for Alice's MoonCat"
    )

    await PORTAL.connect(Alice).approve(Bob.address, KEY_MOONCAT)
    expect(await PORTAL.getApproved(KEY_MOONCAT)).to.equal(
      Bob.address,
      'Bob now approved'
    )

    await PORTAL.connect(Bob).buyAccessory(KEY_MOONCAT, KEY_ACCESSORY, 0, 100, {
      value: price,
    })
  })

  it('should allow designating an approved operator for all owned tokens', async function () {
    const { Alice, Bob, Eve } = await setup(),
      KEY_MOONCAT = moonCatOwners[Alice.address][0],
      KEY_ACCESSORY = 21, // Boutique Shades
      { price } = await ACCESSORIES.accessoryInfo(KEY_ACCESSORY)

    expect(await PORTAL.isApprovedForAll(Alice.address, Bob.address)).to.equal(
      false,
      'Tokens start unapproved'
    )

    await expect(
      PORTAL.connect(Bob).buyAccessory(KEY_MOONCAT, KEY_ACCESSORY, 0, 100, {
        value: price,
      })
    ).to.be.revertedWith(
      'Buy Accessory caller is not owner nor approved',
      "Try to buy an Accessory as Bob, for Alice's MoonCat"
    )

    await PORTAL.connect(Eve).setApprovalForAll(Bob.address, true)
    await expect(
      PORTAL.connect(Bob).buyAccessory(KEY_MOONCAT, KEY_ACCESSORY, 0, 100, {
        value: price,
      })
    ).to.be.revertedWith(
      'Buy Accessory caller is not owner nor approved',
      "Eve's approval has no effect on Alice's MoonCats"
    )

    await PORTAL.connect(Alice).setApprovalForAll(Bob.address, true)
    expect(await PORTAL.isApprovedForAll(Alice.address, Bob.address)).to.equal(
      true,
      'Bob now approved'
    )

    await PORTAL.connect(Bob).buyAccessory(KEY_MOONCAT, KEY_ACCESSORY, 0, 100, {
      value: price,
    })
  })

  it('should allow purchasing by proxy', async function () {
    const { Alice, Bob, Eve } = await setup(),
      KEY_MOONCAT = moonCatOwners[Alice.address][0],
      KEY_ACCESSORY = 21, // Boutique Shades
      { price } = await ACCESSORIES.accessoryInfo(KEY_ACCESSORY)

    // Create an EIP712 message for Alice to sign
    let message = {
      expirationTimestamp: Math.floor(Date.now() / 1000) + 60 * 60 * 24, // A day from now
      nonce: 10,
      purchaseData: {
        rescueOrder: KEY_MOONCAT,
        accessory: KEY_ACCESSORY,
        paletteIndex: 0,
        zIndex: 10,
      },
      tributeToken: ethers.constants.AddressZero,
      tributeAmount: 0,
    }

    // https://docs.ethers.io/v5/api/utils/hashing/#TypedDataEncoder-getPayload
    // Transform the pieces into the structure that would be needed by Metamask to sign
    console.log(
      JSON.stringify(
        ethers.utils._TypedDataEncoder.getPayload(
          EIP721_DOMAIN,
          EIP712_TYPES,
          message
        ),
        null,
        2
      )
    )

    // EIP712 Domain's typeHash is a known constant
    expect(await PORTAL.EIP712DOMAIN_TYPEHASH()).to.equal(
      '0x8b73c3c69bb8fe3d512ecc4cf759cc79239f7b179b0ffacaa9a75d522b39400f',
      'EIP712 Domain typeHash'
    )

    // The EIP712 "domain", for this contract, for this chain, for these actions:
    // https://docs.ethers.io/v5/api/utils/hashing/#TypedDataEncoder-hashDomain
    let domainSeparator =
      ethers.utils._TypedDataEncoder.hashDomain(EIP721_DOMAIN)
    console.log('Domain separator', domainSeparator)
    expect(await PORTAL.DOMAIN_SEPARATOR()).to.equal(
      domainSeparator,
      'Domain Separator'
    )

    // Compare the typeHash values for this contract's custom data types to the standard's computations
    let batchDataHash = typeHash('AccessoryBatchData', EIP712_TYPES)
    console.log('AccessoryBatchData typeHash', batchDataHash)
    expect(await PORTAL.ACCESSORYBATCH_TYPEHASH()).to.equal(
      batchDataHash,
      'AccessoryBatchData typeHash'
    )

    let requestHash = typeHash('AccessoryPurchaseRequest', EIP712_TYPES)
    console.log('AccessoryPurchaseRequest typeHash', requestHash)
    expect(await PORTAL.ACCESSORYREQUEST_TYPEHASH()).to.equal(
      requestHash,
      'AccessoryPurchaseRequest typeHash'
    )

    // Compare logic for hashing individual parts of the message to the standard's computations
    let batchDataObject = structHash(
      'AccessoryBatchData',
      EIP712_TYPES,
      message.purchaseData
    )
    expect(
      await PORTAL['hash((uint256,uint232,uint8,uint16))']([
        KEY_MOONCAT,
        KEY_ACCESSORY,
        0,
        10,
      ])
    ).to.equal(batchDataObject, 'Batch Data object')

    let requestObject = structHash(
      'AccessoryPurchaseRequest',
      EIP712_TYPES,
      message
    )
    expect(
      await PORTAL[
        'hash((uint256,uint256,(uint256,uint232,uint8,uint16),address,uint256))'
      ](message)
    ).to.equal(requestObject, 'Request object')

    // Create a signed message from Alice
    // https://docs.ethers.io/v5/api/utils/hashing/#TypedDataEncoder-hash
    let messageHash = ethers.utils._TypedDataEncoder.hash(
      EIP721_DOMAIN,
      EIP712_TYPES,
      message
    )
    let manualHash = signHash({
      types: {
        ...EIP712_TYPES,
        EIP712Domain: [
          { name: 'name', type: 'string' },
          { name: 'version', type: 'string' },
          { name: 'chainId', type: 'uint256' },
          { name: 'verifyingContract', type: 'address' },
        ],
      },
      domain: EIP721_DOMAIN,
      primaryType: 'AccessoryPurchaseRequest',
      message: message,
    })
    expect(messageHash).to.equal(
      manualHash,
      "Compare EthersJS to standards' example"
    )
    expect(await PORTAL.getProxyMessage(message)).to.equal(
      messageHash,
      'Message payload'
    )

    let aliceSig = await Alice._signTypedData(
      EIP721_DOMAIN,
      EIP712_TYPES,
      message
    )
    expect(
      await PORTAL.validSignature(message, Alice.address, aliceSig)
    ).to.equal(true, 'Alice signature is valid')

    let eveSig = await Eve._signTypedData(EIP721_DOMAIN, EIP712_TYPES, message)
    expect(
      await PORTAL.validSignature(message, Alice.address, eveSig)
    ).to.equal(false, 'Eve signature is invalid')

    await expect(
      PORTAL.connect(Eve).proxyBuyAccessory(message, '0x01020304', {
        value: price,
      })
    ).to.be.revertedWith(
      'Invalid Signature',
      'Try to buy with invalid signature'
    )

    await expect(
      PORTAL.connect(Eve).proxyBuyAccessory(message, eveSig, { value: price })
    ).to.be.revertedWith(
      'Order signature not valid',
      'Try to buy with Eve signature'
    )

    // Bob spends his ETH on gas and purchase price, to get accessory
    // for Alice's MoonCat
    let tx = await PORTAL.connect(Bob).proxyBuyAccessory(message, aliceSig, {
      value: price,
    })
    await gasLedger.gasUsed(
      tx,
      this.test.title,
      'Buy single accessory as a proxy'
    )
    await expect(tx)
      .to.emit(PORTAL, 'ProxyPurchase')
      .withArgs(
        Alice.address,
        Bob.address,
        KEY_MOONCAT,
        KEY_ACCESSORY,
        message.nonce
      )
    await expect(tx)
      .to.emit(ACCESSORIES, 'AccessoryPurchased')
      .withArgs(KEY_ACCESSORY, KEY_MOONCAT, price)

    expect(
      await ACCESSORIES.doesMoonCatOwnAccessory(KEY_MOONCAT, KEY_ACCESSORY)
    ).to.equal(true, 'Now owns')

    // Attempt a "replay attack"
    await expect(
      PORTAL.connect(Bob).proxyBuyAccessory(message, aliceSig, { value: price })
    ).to.be.revertedWith('Already Owned', 'Replay same purchase request')
  })

  it('should allow purchasing multiple by proxy', async function () {
    const { Alice, Bob, Eve } = await setup(),
      KEY_MOONCAT = moonCatOwners[Alice.address][0],
      accessories = [20, 21]

    let prices = []
    for (accessory of accessories) {
      let { price } = await ACCESSORIES.accessoryInfo(accessory)
      prices.push(price)
    }

    // Create EIP712 messages for Alice to sign
    let aDayFromNow = Math.floor(Date.now() / 1000) + 60 * 60 * 24
    let messages = accessories.map((accessory) => {
      return {
        expirationTimestamp: aDayFromNow,
        nonce: 10,
        purchaseData: {
          rescueOrder: KEY_MOONCAT,
          accessory: accessory,
          paletteIndex: 0,
          zIndex: 10,
        },
        tributeToken: ethers.constants.AddressZero,
        tributeAmount: 0,
      }
    })

    let signatures = []
    for (message of messages) {
      let signature = await Alice._signTypedData(
        EIP721_DOMAIN,
        EIP712_TYPES,
        message
      )
      signatures.push(signature)
    }

    let tx = await PORTAL.connect(Bob).proxyBuyAccessories(
      messages,
      signatures,
      { value: prices[0].add(prices[1]) }
    )
    await gasLedger.gasUsed(
      tx,
      this.test.title,
      'Buy two accessories as a proxy'
    )
    await expect(tx)
      .to.emit(PORTAL, 'ProxyPurchase')
      .withArgs(
        Alice.address,
        Bob.address,
        KEY_MOONCAT,
        accessories[0],
        messages[0].nonce
      )
    await expect(tx)
      .to.emit(PORTAL, 'ProxyPurchase')
      .withArgs(
        Alice.address,
        Bob.address,
        KEY_MOONCAT,
        accessories[1],
        messages[1].nonce
      )
    await expect(tx)
      .to.emit(ACCESSORIES, 'AccessoryPurchased')
      .withArgs(accessories[0], KEY_MOONCAT, prices[0])
    await expect(tx)
      .to.emit(ACCESSORIES, 'AccessoryPurchased')
      .withArgs(accessories[1], KEY_MOONCAT, prices[1])

    expect(
      await ACCESSORIES.doesMoonCatOwnAccessory(KEY_MOONCAT, accessories[0])
    ).to.equal(true, 'Now owns')
    expect(
      await ACCESSORIES.doesMoonCatOwnAccessory(KEY_MOONCAT, accessories[1])
    ).to.equal(true, 'Now owns')
  })

  it('should reject invalid proxy requests', async function () {
    const { Alice, Bob, Eve } = await setup(),
      KEY_MOONCAT = moonCatOwners[Alice.address][0],
      KEY_ACCESSORY = 21, // Boutique Shades
      { price } = await ACCESSORIES.accessoryInfo(KEY_ACCESSORY)

    let currentBlock = await ethers.provider.getBlock('latest')
    let timestampNow = currentBlock.timestamp

    // Create an EIP712 message for Alice to sign
    let aDayFromNow = Math.floor(Date.now() / 1000) + 60 * 60 * 24
    const MESSAGE_TEMPLATE = {
      expirationTimestamp: aDayFromNow,
      nonce: 10,
      purchaseData: {
        rescueOrder: KEY_MOONCAT,
        accessory: KEY_ACCESSORY,
        paletteIndex: 0,
        zIndex: 10,
      },
      tributeToken: ethers.constants.AddressZero,
      tributeAmount: 0,
    }

    testMessage = {
      ...MESSAGE_TEMPLATE,
      expirationTimestamp: timestampNow - 500,
    }
    aliceSig = await Alice._signTypedData(
      EIP721_DOMAIN,
      EIP712_TYPES,
      testMessage
    )
    await expect(
      PORTAL.connect(Bob).proxyBuyAccessory(testMessage, aliceSig, {
        value: price,
      })
    ).to.be.revertedWith('Order has expired', 'Purchase with expired order')

    testMessage = { ...MESSAGE_TEMPLATE }
    testMessage.purchaseData.rescueOrder = moonCatOwners[Alice.address][1]
    let signatures = [
      await Alice._signTypedData(EIP721_DOMAIN, EIP712_TYPES, MESSAGE_TEMPLATE),
      await Alice._signTypedData(EIP721_DOMAIN, EIP712_TYPES, testMessage),
    ]
    await expect(
      PORTAL.connect(Bob).proxyBuyAccessories(
        [MESSAGE_TEMPLATE, testMessage],
        signatures,
        { value: price.add(price) }
      )
    ).to.be.revertedWith(
      'Already Owned',
      'Multi-purchase with mismatched MoonCat IDs'
    )

    expect(await PORTAL.validNonce(Alice.address)).to.equal(
      0,
      'Valid nonce starts at zero'
    )
    await PORTAL.connect(Alice).setValidNonce(15)
    expect(await PORTAL.validNonce(Alice.address)).to.equal(
      15,
      'Valid nonce updated'
    )

    aliceSig = await Alice._signTypedData(
      EIP721_DOMAIN,
      EIP712_TYPES,
      MESSAGE_TEMPLATE
    )
    await expect(
      PORTAL.connect(Bob).proxyBuyAccessory(MESSAGE_TEMPLATE, aliceSig, {
        value: price,
      })
    ).to.be.revertedWith('Order nonce not valid', 'Purchase with invalid nonce')

    testMessage = {
      ...MESSAGE_TEMPLATE,
      nonce: 15,
    }
    aliceSig = await Alice._signTypedData(
      EIP721_DOMAIN,
      EIP712_TYPES,
      testMessage
    )
    await PORTAL.connect(Bob).proxyBuyAccessory(testMessage, aliceSig, {
      value: price,
    }) // Should be successful, since nonce was updated
  })

  it('should properly process tribute token amounts', async function () {
    const { Alice, Bob, Eve } = await setup(),
      KEY_MOONCAT = moonCatOwners[Alice.address][0],
      KEY_ACCESSORY = 21, // Boutique Shades
      { price } = await ACCESSORIES.accessoryInfo(KEY_ACCESSORY)

    // Create an EIP712 message for Alice to sign
    let message = {
      expirationTimestamp: Math.floor(Date.now() / 1000) + 60 * 60 * 24, // A day from now
      nonce: 10,
      purchaseData: {
        rescueOrder: KEY_MOONCAT,
        accessory: KEY_ACCESSORY,
        paletteIndex: 0,
        zIndex: 10,
      },
      tributeToken: DAI.address,
      tributeAmount: 5000,
    }

    let aliceSig = await Alice._signTypedData(
      EIP721_DOMAIN,
      EIP712_TYPES,
      message
    )

    await expect(
      PORTAL.connect(Bob).proxyBuyAccessory(message, aliceSig, { value: price })
    ).to.be.revertedWith(
      'Dai/insufficient-balance',
      'Purchase without any funds'
    )

    await DAI.connect(DAI_HOLDER).transfer(Alice.address, 10000)

    await expect(
      PORTAL.connect(Bob).proxyBuyAccessory(message, aliceSig, { value: price })
    ).to.be.revertedWith(
      'Dai/insufficient-allowance',
      'Purchase without any funds'
    )

    await DAI.connect(Alice).approve(PORTAL.address, 6000)

    await expect(() =>
      PORTAL.connect(Bob).proxyBuyAccessory(message, aliceSig, { value: price })
    ).to.changeTokenBalances(DAI.connect(Alice), [Alice, Bob], [-5000, 5000])
  })
})

async function setup() {
  const accounts = await ethers.getSigners()
  const Alice = accounts[1]
  const Bob = accounts[2]
  const Carol = accounts[3]
  const Deb = accounts[0]
  const Eve = accounts[8]

  // Put all of Alice's MoonCats into JumpPort
  await ACCLIMATOR.connect(Alice).setApprovalForAll(JUMPPORT.address, true)
  await JUMPPORT.connect(Alice)['deposit(address,uint256[])'](
    ACCLIMATOR.address,
    moonCatOwners[Alice.address]
  )
  return { Alice, Bob, Carol, Deb, Eve }
}

async function updateMoonCatOwners(startIndex = 1000, endIndex = 1049) {
  let out = {}
  for (let i = startIndex; i <= endIndex; i++) {
    let owner = await ACCLIMATOR.ownerOf(i)
    if (typeof out[owner] == 'undefined') out[owner] = []
    out[owner].push(i)
  }
  return out
}

// EIP712 manual signing
// https://github.com/ethereum/EIPs/blob/master/assets/eip-712/Example.js

function dependencies(types, primaryType, found = []) {
  if (found.includes(primaryType)) {
    return found
  }
  if (types[primaryType] === undefined) {
    return found
  }
  found.push(primaryType)
  for (let field of types[primaryType]) {
    for (let dep of dependencies(types, field.type, found)) {
      if (!found.includes(dep)) {
        found.push(dep)
      }
    }
  }
  return found
}

function encodeType(primaryType, types) {
  // Get dependencies primary first, then alphabetical
  let deps = dependencies(types, primaryType)
  deps = deps.filter((t) => t != primaryType)
  deps = [primaryType].concat(deps.sort())

  // Format as a string with fields
  let result = ''
  for (let type of deps) {
    result += `${type}(${types[type]
      .map(({ name, type }) => `${type} ${name}`)
      .join(',')})`
  }
  return result
}

function typeHash(primaryType, types) {
  return ethers.utils.keccak256(
    ethers.utils.toUtf8Bytes(encodeType(primaryType, types))
  )
}

function encodeData(primaryType, types, data) {
  let encTypes = []
  let encValues = []

  // Add typehash
  encTypes.push('bytes32')
  encValues.push(typeHash(primaryType, types))

  // Add field contents
  for (let field of types[primaryType]) {
    let value = data[field.name]
    if (field.type == 'string' || field.type == 'bytes') {
      encTypes.push('bytes32')
      value = ethers.utils.keccak256(ethers.utils.toUtf8Bytes(value))
      encValues.push(value)
    } else if (types[field.type] !== undefined) {
      encTypes.push('bytes32')
      value = ethers.utils.keccak256(encodeData(field.type, types, value))
      encValues.push(value)
    } else if (field.type.lastIndexOf(']') === field.type.length - 1) {
      throw 'TODO: Arrays currently unimplemented in encodeData'
    } else {
      encTypes.push(field.type)
      encValues.push(value)
    }
  }

  return abiCoder.encode(encTypes, encValues)
}

function structHash(primaryType, types, data) {
  return ethers.utils.keccak256(encodeData(primaryType, types, data))
}

function signHash(typedData) {
  return ethers.utils.keccak256(
    Buffer.concat([
      Buffer.from('1901', 'hex'),
      Buffer.from(
        structHash('EIP712Domain', typedData.types, typedData.domain).substr(2),
        'hex'
      ),
      Buffer.from(
        structHash(
          typedData.primaryType,
          typedData.types,
          typedData.message
        ).substr(2),
        'hex'
      ),
    ])
  )
}
