require('dotenv').config()
const { network, ethers } = require('hardhat'),
  gasLedger = require('../lib/gasLedger'),
  {
    JumpPort,
    MoonCatAcclimator,
    disperseMoonCats,
  } = require('@mooncatrescue/contracts/moonCatUtils'),
  { expect } = require('chai')

let currentSnapshot = false
let moonCatOwners = {}

let ACCLIMATOR = false
let JUMPPORT = false

describe('MoonCat JumpPort basic logic', function () {
  before(async function () {
    const accounts = await ethers.getSigners()
    ACCLIMATOR = await MoonCatAcclimator
    JUMPPORT = await JumpPort

    // Set first Hardhat account as an admin of the JumpPort
    const jumpportAdmin = '0xdf2E60Af57C411F848B1eA12B10a404d194bce27'
    await accounts[5].sendTransaction({
      to: jumpportAdmin,
      value: ethers.utils.parseEther('0.3'),
    })
    await network.provider.request({
      method: 'hardhat_impersonateAccount',
      params: [jumpportAdmin],
    })
    let adminSigner = await ethers.provider.getSigner(jumpportAdmin)
    await JUMPPORT.connect(adminSigner).setAdmin(accounts[0].address, true)

    if (network.name == 'hardhat') {
      // Set up test accounts as the owners of several cats
      this.timeout(300000)
      await disperseMoonCats(1000, 1020)
    }
    moonCatOwners = await updateMoonCatOwners(1000, 1020)
  })
  after(function () {
    gasLedger.reportGasUsage()
    gasLedger.exportLedger(__filename)
  })

  beforeEach(async function () {
    currentSnapshot = await network.provider.request({
      method: 'evm_snapshot',
      params: [],
    })
  })
  afterEach(async function () {
    await network.provider.request({
      method: 'evm_revert',
      params: [currentSnapshot],
    })
  })

  it('should track locking status across portal fuzzing', async function () {
    const { accounts, ownedMoonCats } = await setup()

    // Use manual wallets to emulate Portals
    const portals = [
      accounts[4],
      accounts[5],
      accounts[6],
      accounts[7],
      accounts[8],
      accounts[9],
    ]
    let lockStatus = {}
    let testCoverage = {}
    for (let i = 0; i < portals.length; i++) {
      await JUMPPORT.setPortalValidation(portals[i].address, true)
      lockStatus[i] = false
      testCoverage[i] = 0
    }
    testCoverage[portals.length] = 0
    const KEY_MOONCAT = ownedMoonCats[0]

    this.timeout(150000)
    for (let round = 0; round < 1000; round++) {
      if (round % 50 == 0) console.log('Fuzzing round', round)

      // Pick a random portal to toggle its state
      let portalIndex = Math.floor(Math.random() * portals.length)

      // Flip that portals stake in our mapping of expected results
      lockStatus[portalIndex] = lockStatus[portalIndex] == true ? false : true

      // Take that action on the JumpPort as that Portal
      if (lockStatus[portalIndex] == true) {
        await JUMPPORT.connect(portals[portalIndex]).lockToken(
          ACCLIMATOR.address,
          KEY_MOONCAT
        )
        //console.log(`Portal ${portalIndex} locked`);
      } else {
        await JUMPPORT.connect(portals[portalIndex]).unlockToken(
          ACCLIMATOR.address,
          KEY_MOONCAT
        )
        //console.log(`Portal ${portalIndex} unlocked`);
      }

      // What portals we expect to be locked at this point
      let expected = portals
        .filter((signer, index) => lockStatus[index])
        .map((signer) => signer.address)
      //console.log(`${expected.length} portals actively locked`);

      // Verify contract metadata is correct
      testCoverage[expected.length]++
      expect(
        await JUMPPORT.lockedBy(ACCLIMATOR.address, KEY_MOONCAT)
      ).to.have.members(expected, 'Portal locks enumerated correctly')
      if (expected.length > 0) {
        expect(
          await JUMPPORT.isLocked(ACCLIMATOR.address, KEY_MOONCAT)
        ).to.equal(true, 'Token marked as locked')
      } else {
        expect(
          await JUMPPORT.isLocked(ACCLIMATOR.address, KEY_MOONCAT)
        ).to.equal(false, 'Token marked as unlocked')
      }
    }

    console.log('Fuzzing coverage:')
    for (let i = 0; i <= portals.length; i++) {
      console.log(
        `${i} Portals locked at once was tested ${testCoverage[i]} times.`
      )
    }
  })
})

async function updateMoonCatOwners(startIndex = 1000, endIndex = 1049) {
  let out = {}
  for (let i = startIndex; i <= endIndex; i++) {
    let owner = await ACCLIMATOR.ownerOf(i)
    if (typeof out[owner] == 'undefined') out[owner] = []
    out[owner].push(i)
  }
  return out
}

async function setup() {
  const accounts = await ethers.getSigners()
  const moonCatOwner = accounts[1]

  // Put all owned MoonCats into JumpPort
  await depositAllOwned(moonCatOwner)

  return {
    moonCatOwner: moonCatOwner,
    EVE: accounts[8],
    ownedMoonCats: moonCatOwners[moonCatOwner.address],
    accounts: accounts,
  }
}

async function depositAllOwned(signer) {
  await ACCLIMATOR.connect(signer).setApprovalForAll(JUMPPORT.address, true)
  await JUMPPORT.connect(signer)['deposit(address,uint256[])'](
    ACCLIMATOR.address,
    moonCatOwners[signer.address]
  )
}
