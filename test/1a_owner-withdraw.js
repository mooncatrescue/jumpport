require('dotenv').config()
const { network, ethers } = require('hardhat'),
  gasLedger = require('../lib/gasLedger'),
  {
    JumpPort,
    MoonCatAcclimator,
    disperseMoonCats,
    expectPermissionError,
  } = require('@mooncatrescue/contracts/moonCatUtils'),
  { expect } = require('chai')

let currentSnapshot = false
let moonCatOwners = {}

let ACCLIMATOR = false
let JUMPPORT = false

describe('MoonCat JumpPort contract owner ERC721 token rescuing', function () {
  before(async function () {
    const accounts = await ethers.getSigners()
    ACCLIMATOR = await MoonCatAcclimator
    JUMPPORT = await JumpPort

    // Set first Hardhat account as an admin of the JumpPort
    const jumpportAdmin = '0xdf2E60Af57C411F848B1eA12B10a404d194bce27'
    await accounts[5].sendTransaction({
      to: jumpportAdmin,
      value: ethers.utils.parseEther('0.3'),
    })
    await network.provider.request({
      method: 'hardhat_impersonateAccount',
      params: [jumpportAdmin],
    })
    let adminSigner = await ethers.provider.getSigner(jumpportAdmin)
    await JUMPPORT.connect(adminSigner).setAdmin(accounts[0].address, true)

    if (network.name == 'hardhat') {
      // Set up test accounts as the owners of several cats
      this.timeout(300000)
      await disperseMoonCats(1000, 1020)
    }
    moonCatOwners = await updateMoonCatOwners(1000, 1020)
  })
  after(function () {
    gasLedger.reportGasUsage()
    gasLedger.exportLedger(__filename)
  })

  beforeEach(async function () {
    currentSnapshot = await network.provider.request({
      method: 'evm_snapshot',
      params: [],
    })
  })
  afterEach(async function () {
    await network.provider.request({
      method: 'evm_revert',
      params: [currentSnapshot],
    })
  })

  it('should not allow validly-deposited tokens to be withdrawn immediately', async function () {
    const accounts = await ethers.getSigners()
    const moonCatOwner = accounts[1],
      EVE = accounts[8],
      ownedMoonCats = moonCatOwners[moonCatOwner.address]

    const ADMIN_ROLE = await JUMPPORT.ADMIN_ROLE()

    // Put one MoonCat into JumpPort
    await ACCLIMATOR.connect(moonCatOwner)[
      'safeTransferFrom(address,address,uint256)'
    ](moonCatOwner.address, JUMPPORT.address, ownedMoonCats[0])

    await expect(
      JUMPPORT.withdrawForeignERC721(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.be.revertedWith(
      'Owner ping has not expired',
      'Attempt to force-withdraw valid token'
    )
    await expectPermissionError(
      JUMPPORT.connect(EVE).withdrawForeignERC721(
        ACCLIMATOR.address,
        ownedMoonCats[0]
      ),
      ADMIN_ROLE,
      EVE.address
    )

    // Try to request owner ping-pong response as non-contract-owner
    await expectPermissionError(
      JUMPPORT.connect(EVE).adminWithdrawPing(
        ACCLIMATOR.address,
        ownedMoonCats[0]
      ),
      ADMIN_ROLE,
      EVE.address
    )

    // Request owner ping-pong response
    let tx = await JUMPPORT.adminWithdrawPing(
      ACCLIMATOR.address,
      ownedMoonCats[0]
    )
    await gasLedger.gasUsed(
      tx,
      this.test.title,
      'Request token-owner ping-pong response'
    )
    let block = await ethers.provider.getBlock('latest')
    expect(
      await JUMPPORT.tokenPingRequestBlock(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.equal(block.number, 'Ping request block recorded')

    let rs = await JUMPPORT.tokenPingRequestBlocks(
      [ACCLIMATOR.address, ACCLIMATOR.address],
      [ownedMoonCats[1], ownedMoonCats[0]]
    )
    expect(rs.length).to.equal(2, 'Expected number of responses sent')
    expect(rs[1]).to.equal(block.number, 'Ping request block recorded')
    expect(rs[0]).to.equal(0, 'No ping request for other tokens')

    await expect(
      JUMPPORT.withdrawForeignERC721(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.be.revertedWith(
      'Owner ping has not expired',
      'Right after setting ping block is not valid'
    )

    await network.provider.send('hardhat_mine', ['0xff'])

    await expect(
      JUMPPORT.withdrawForeignERC721(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.be.revertedWith(
      'Owner ping has not expired',
      'Right after setting ping block is not valid'
    )

    await network.provider.send('hardhat_mine', [
      ethers.BigNumber.from('2400000').toHexString(),
    ])

    await testBalance(ACCLIMATOR.address, moonCatOwner.address, 1)
    await JUMPPORT.withdrawForeignERC721(ACCLIMATOR.address, ownedMoonCats[0])
    expect(await ACCLIMATOR.ownerOf(ownedMoonCats[0])).to.equal(
      accounts[0].address,
      'Contract owner is new owner'
    )
    await expect(
      JUMPPORT.ownerOf(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.be.revertedWith(
      'Not currently deposited',
      'MoonCat not marked as deposited, though'
    )
    await testBalance(ACCLIMATOR.address, moonCatOwner.address, 0)
  })

  it('should allow contract owner to withdraw improperly-deposited tokens', async function () {
    const accounts = await ethers.getSigners()
    const moonCatOwner = accounts[1],
      EVE = accounts[8],
      ownedMoonCats = moonCatOwners[moonCatOwner.address]

    // Put one MoonCat into JumpPort without using safe transfer notification
    await ACCLIMATOR.connect(moonCatOwner)[
      'transferFrom(address,address,uint256)'
    ](moonCatOwner.address, JUMPPORT.address, ownedMoonCats[0])

    expect(await ACCLIMATOR.ownerOf(ownedMoonCats[0])).to.equal(
      JUMPPORT.address,
      'Acclimator contract recorded new owner'
    )
    await expect(
      JUMPPORT.ownerOf(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.be.revertedWith(
      'Not currently deposited',
      'MoonCat not marked as deposited, though'
    )
    await testBalance(ACCLIMATOR.address, moonCatOwner.address, 0)

    const ADMIN_ROLE = await JUMPPORT.ADMIN_ROLE()
    await expectPermissionError(
      JUMPPORT.connect(EVE).withdrawForeignERC721(
        ACCLIMATOR.address,
        ownedMoonCats[0]
      ),
      ADMIN_ROLE,
      EVE.address
    )

    // Attempt to rescue as contract owner
    await JUMPPORT.withdrawForeignERC721(ACCLIMATOR.address, ownedMoonCats[0])
    expect(await ACCLIMATOR.ownerOf(ownedMoonCats[0])).to.equal(
      accounts[0].address,
      'Contract owner is new owner'
    )
    await expect(
      JUMPPORT.ownerOf(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.be.revertedWith(
      'Not currently deposited',
      'MoonCat still not deposited'
    )
    await testBalance(ACCLIMATOR.address, moonCatOwner.address, 0)
  })

  it('should allow token owner to cancel a contract-owner withdraw request', async function () {
    const accounts = await ethers.getSigners()
    const moonCatOwner = accounts[1],
      EVE = accounts[8],
      ownedMoonCats = moonCatOwners[moonCatOwner.address]

    // Put one MoonCat into JumpPort
    await ACCLIMATOR.connect(moonCatOwner)[
      'safeTransferFrom(address,address,uint256)'
    ](moonCatOwner.address, JUMPPORT.address, ownedMoonCats[0])

    await expect(
      JUMPPORT.withdrawForeignERC721(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.be.revertedWith(
      'Owner ping has not expired',
      'Attempt to force-withdraw valid token'
    )

    // Request owner ping-pong response
    await JUMPPORT.adminWithdrawPing(ACCLIMATOR.address, ownedMoonCats[0])
    let block = await ethers.provider.getBlock('latest')
    expect(
      await JUMPPORT.tokenPingRequestBlock(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.equal(block.number, 'Ping request block recorded')

    // Jump forward a year...
    await network.provider.send('hardhat_mine', [
      ethers.BigNumber.from('2400000').toHexString(),
    ])

    await expect(
      JUMPPORT.connect(EVE).ownerPong(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.be.revertedWith(
      'Not an operator of that token',
      'Attempt to cancel as non-owner'
    )

    // Cancel request as token owner
    let tx = await JUMPPORT.connect(moonCatOwner).ownerPong(
      ACCLIMATOR.address,
      ownedMoonCats[0]
    )
    await gasLedger.gasUsed(
      tx,
      this.test.title,
      'Token-owner ping-pong response'
    )
    expect(
      await JUMPPORT.tokenPingRequestBlock(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.equal(0, 'Ping request block reset')

    await expect(
      JUMPPORT.withdrawForeignERC721(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.be.revertedWith('Owner ping has not expired', 'Ping time reset')
  })

  it('should allow cancelling an administrative withdraw by the owner withdrawing', async function () {
    const accounts = await ethers.getSigners()
    let moonCatOwner = accounts[1]
    let ownedMoonCats = moonCatOwners[moonCatOwner.address]

    // Put one MoonCat into JumpPort
    await ACCLIMATOR.connect(moonCatOwner)[
      'safeTransferFrom(address,address,uint256)'
    ](moonCatOwner.address, JUMPPORT.address, ownedMoonCats[0])

    // Request owner ping-pong response
    await JUMPPORT.adminWithdrawPing(ACCLIMATOR.address, ownedMoonCats[0])
    let block = await ethers.provider.getBlock('latest')
    expect(await ACCLIMATOR.ownerOf(ownedMoonCats[0])).to.equal(
      JUMPPORT.address,
      'MoonCat is deposited'
    )
    expect(
      await JUMPPORT.ownerOf(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.equal(moonCatOwner.address, 'Former owner is owner in JumpPort')

    // Withdraw the MoonCat as the owner
    await JUMPPORT.connect(moonCatOwner)['safeWithdraw(address,uint256)'](
      ACCLIMATOR.address,
      ownedMoonCats[0]
    )
    expect(
      await JUMPPORT.tokenPingRequestBlock(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.equal(block.number, 'Ping request block still set')
    expect(await ACCLIMATOR.ownerOf(ownedMoonCats[0])).to.equal(
      moonCatOwner.address
    )

    // Put back in the JumpPort
    await ACCLIMATOR.connect(moonCatOwner)[
      'safeTransferFrom(address,address,uint256)'
    ](moonCatOwner.address, JUMPPORT.address, ownedMoonCats[0])

    expect(
      await JUMPPORT.tokenPingRequestBlock(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.equal(0, 'Ping request block reset')

    // Request ping-pong response again
    await JUMPPORT.adminWithdrawPing(ACCLIMATOR.address, ownedMoonCats[0])
    block = await ethers.provider.getBlock('latest')

    // Withdraw the MoonCat as the owner
    await JUMPPORT.connect(moonCatOwner)['safeWithdraw(address,uint256)'](
      ACCLIMATOR.address,
      ownedMoonCats[0]
    )

    // Jump forward a year...
    await network.provider.send('hardhat_mine', [
      ethers.BigNumber.from('2400000').toHexString(),
    ])

    // Token is already withdrawn, so cannot withdraw
    await expect(
      JUMPPORT.withdrawForeignERC721(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.be.revertedWith(
      'ERC721: transfer caller is not owner nor approved',
      'Ping time reset'
    )
  })
})

async function updateMoonCatOwners(startIndex = 1000, endIndex = 1049) {
  let out = {}
  for (let i = startIndex; i <= endIndex; i++) {
    let owner = await ACCLIMATOR.ownerOf(i)
    if (typeof out[owner] == 'undefined') out[owner] = []
    out[owner].push(i)
  }
  return out
}

async function testBalance(
  tokenCollection,
  targetAddress,
  expectedBalance,
  expectedBlockHeight = null
) {
  if (Array.isArray(tokenCollection)) {
    expect(
      await JUMPPORT['balanceOf(address[],address)'](
        tokenCollection,
        targetAddress
      )
    ).to.equal(expectedBalance)
  } else {
    let rs = await JUMPPORT['balanceOf(address,address)'](
      tokenCollection,
      targetAddress
    )
    expect(rs).to.have.property('balance')
    expect(rs.balance.toHexString()).to.equal(
      ethers.BigNumber.from(expectedBalance).toHexString()
    )
    if (expectedBlockHeight != null) {
      expect(rs).to.have.property('blockHeight')
      expect(rs.blockHeight.toHexString()).to.equal(
        ethers.BigNumber.from(expectedBlockHeight).toHexString()
      )
    }
  }
}
