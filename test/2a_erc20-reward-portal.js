require('dotenv').config()
const { network, ethers } = require('hardhat'),
  gasLedger = require('../lib/gasLedger'),
  {
    MoonCatAcclimator,
    JumpPort,
    disperseMoonCats,
    expectPermissionError,
  } = require('@mooncatrescue/contracts/moonCatUtils'),
  { expect } = require('chai')

let currentSnapshot = false
let moonCatOwners = {}

let ACCLIMATOR = false
let JUMPPORT = false
let PORTAL = false

const DAI = new ethers.Contract('0x6b175474e89094c44da98b954eedeac495271d0f', [
  'function balanceOf(address account) external view returns (uint256)',
  'function transfer(address recipient, uint256 amount) external returns (bool)',
])
let DAI_HOLDER = false

describe('ERC20 Staking Portal basic functionality', function () {
  before(async function () {
    const accounts = await ethers.getSigners()
    ACCLIMATOR = await MoonCatAcclimator
    JUMPPORT = await JumpPort

    // Set first Hardhat account as an admin of the JumpPort
    const jumpportAdmin = '0xdf2E60Af57C411F848B1eA12B10a404d194bce27'
    await accounts[5].sendTransaction({
      to: jumpportAdmin,
      value: ethers.utils.parseEther('0.3'),
    })
    await network.provider.request({
      method: 'hardhat_impersonateAccount',
      params: [jumpportAdmin],
    })
    let adminSigner = await ethers.provider.getSigner(jumpportAdmin)
    await JUMPPORT.connect(adminSigner).setAdmin(accounts[0].address, true)

    let daiHolderAddress = '0x8EB8a3b98659Cce290402893d0123abb75E3ab28'
    DAI_HOLDER = await ethers.provider.getSigner(daiHolderAddress)
    await hre.network.provider.request({
      method: 'hardhat_impersonateAccount',
      params: [daiHolderAddress],
    })

    if (network.name == 'hardhat') {
      // Set up test accounts as the owners of several cats
      this.timeout(300000)
      await disperseMoonCats(1000, 1020)
    }
    moonCatOwners = await updateMoonCatOwners(1000, 1020)

    const StakingPortal = await ethers.getContractFactory('ERC20StakingPortal')
    PORTAL = await StakingPortal.deploy(JUMPPORT.address)
    //console.log('StakingPortal Contract Deployed To:', PORTAL.address);

    await JUMPPORT.setPortalValidation(PORTAL.address, true)
  })
  after(function () {
    gasLedger.reportGasUsage()
    gasLedger.exportLedger(__filename)
  })

  beforeEach(async function () {
    currentSnapshot = await network.provider.request({
      method: 'evm_snapshot',
      params: [],
    })
  })
  afterEach(async function () {
    await network.provider.request({
      method: 'evm_revert',
      params: [currentSnapshot],
    })
  })

  it('should only allow setting up staking by the contract owner', async function () {
    const accounts = await ethers.getSigners()

    // Default to disabled
    expect(await PORTAL.stakingActive()).to.equal(
      false,
      'Staking starts as disabled'
    )

    // Action by non-contract-owner should revert
    const ADMIN_ROLE = await PORTAL.ADMIN_ROLE()
    await expectPermissionError(
      PORTAL.connect(accounts[5]).setTargetTokens(
        ethers.constants.AddressZero,
        ACCLIMATOR.address
      ),
      ADMIN_ROLE,
      accounts[5].address
    )

    await expectPermissionError(
      PORTAL.connect(accounts[5]).setGlobalStart(12345),
      ADMIN_ROLE,
      accounts[5].address
    )
    await expectPermissionError(
      PORTAL.connect(accounts[5]).setRewardTiers([50, 100], [5, 10]),
      ADMIN_ROLE,
      accounts[5].address
    )
    await expectPermissionError(
      PORTAL.connect(accounts[5]).setEnabled(true),
      ADMIN_ROLE,
      accounts[5].address
    )

    // Actions by contract-owner should not revert
    await PORTAL.setTargetTokens(
      ethers.constants.AddressZero,
      ACCLIMATOR.address
    )
    await PORTAL.setGlobalStart(12345)
    await PORTAL.setRewardTiers([50, 100], [5, 10])
    await PORTAL.setEnabled(true)

    expect(await PORTAL.connect(accounts[5]).StakedToken()).to.equal(
      ACCLIMATOR.address,
      'Staked Token address recorded'
    )
    expect(await PORTAL.connect(accounts[5]).stakingActive()).to.equal(
      true,
      'Staking enabled'
    )
    expect(await PORTAL.connect(accounts[5]).getRewardAmount(1)).to.equal(
      0,
      'Reward tier logic accurate'
    )
    expect(await PORTAL.connect(accounts[5]).getRewardAmount(49)).to.equal(
      0,
      'Reward tier logic accurate'
    )
    expect(await PORTAL.connect(accounts[5]).getRewardAmount(50)).to.equal(
      5,
      'Reward tier logic accurate'
    )
    expect(await PORTAL.connect(accounts[5]).getRewardAmount(51)).to.equal(
      5,
      'Reward tier logic accurate'
    )
    expect(await PORTAL.connect(accounts[5]).getRewardAmount(99)).to.equal(
      5,
      'Reward tier logic accurate'
    )
    expect(await PORTAL.connect(accounts[5]).getRewardAmount(100)).to.equal(
      10,
      'Reward tier logic accurate'
    )
    expect(await PORTAL.connect(accounts[5]).getRewardAmount(101)).to.equal(
      10,
      'Reward tier logic accurate'
    )
    expect(await PORTAL.connect(accounts[5]).getRewardAmount(500)).to.equal(
      10,
      'Reward tier logic accurate'
    )

    // Contract owner can update
    await PORTAL.setRewardTiers([5, 10], [5, 10])
    expect(await PORTAL.connect(accounts[5]).getRewardAmount(1)).to.equal(
      0,
      'Reward tier logic accurate'
    )
    expect(await PORTAL.connect(accounts[5]).getRewardAmount(4)).to.equal(
      0,
      'Reward tier logic accurate'
    )
    expect(await PORTAL.connect(accounts[5]).getRewardAmount(5)).to.equal(
      5,
      'Reward tier logic accurate'
    )
    expect(await PORTAL.connect(accounts[5]).getRewardAmount(6)).to.equal(
      5,
      'Reward tier logic accurate'
    )
    expect(await PORTAL.connect(accounts[5]).getRewardAmount(9)).to.equal(
      5,
      'Reward tier logic accurate'
    )
    expect(await PORTAL.connect(accounts[5]).getRewardAmount(10)).to.equal(
      10,
      'Reward tier logic accurate'
    )
    expect(await PORTAL.connect(accounts[5]).getRewardAmount(11)).to.equal(
      10,
      'Reward tier logic accurate'
    )
    expect(await PORTAL.connect(accounts[5]).getRewardAmount(500)).to.equal(
      10,
      'Reward tier logic accurate'
    )

    await PORTAL.setEnabled(false)
    expect(await PORTAL.connect(accounts[5]).stakingActive()).to.equal(
      false,
      'Staking disabled'
    )
  })

  it('should properly lock and unlock tokens', async function () {
    const accounts = await ethers.getSigners()
    let moonCatOwner = accounts[1]
    let ownedMoonCats = moonCatOwners[moonCatOwner.address]

    // Deposit all owned MoonCats into JumpPort
    await ACCLIMATOR.connect(moonCatOwner).setApprovalForAll(
      JUMPPORT.address,
      true
    )
    await JUMPPORT.connect(moonCatOwner)['deposit(address,uint256[])'](
      ACCLIMATOR.address,
      ownedMoonCats
    )
    let block = await ethers.provider.getBlock('latest')
    expect(
      await JUMPPORT.depositedSince(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.equal(block.number, 'Deposit block recorded')

    await expect(
      PORTAL.connect(moonCatOwner).stakeToken(ownedMoonCats[0])
    ).to.be.revertedWith(
      'Staking rewards not configured',
      'Attempt to stake before setup'
    )

    // Configure Portal
    await PORTAL.setTargetTokens(DAI.address, ACCLIMATOR.address)
    await PORTAL.setRewardTiers([50, 100], [5, 10])
    await PORTAL.setEnabled(true)

    // Actions by non-owners should fail
    await expect(
      PORTAL.connect(accounts[5]).stakeToken(ownedMoonCats[0])
    ).to.be.revertedWith(
      'Not an operator of that token',
      'Attempt to stake as non-owner'
    )
    await expect(
      PORTAL.connect(accounts[5]).stakeToken(5000)
    ).to.be.revertedWith(
      'Token not in JumpPort',
      'Attempt to stake non-deposited token'
    )

    // Stake the token
    await expect(PORTAL.connect(moonCatOwner).stakeToken(ownedMoonCats[0]))
      .to.emit(JUMPPORT, 'Lock')
      .withArgs(
        PORTAL.address,
        moonCatOwner.address,
        ACCLIMATOR.address,
        ownedMoonCats[0]
      )

    await network.provider.send('hardhat_mine', ['0xa']) // Jump forward 10 blocks

    // Check staked metadata
    let rs = await PORTAL.getStakedDuration(ownedMoonCats[0])
    expect(rs.startBlock).to.equal(block.number, 'Starting block recorded')
    expect(rs.duration).to.equal(17, 'Staked duration accurately-calculated')

    expect(
      await JUMPPORT.isLocked(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.equal(true, 'MoonCat is now locked')

    // Actions by non-owners should fail
    await expect(
      PORTAL.connect(accounts[5]).claimReward(ownedMoonCats[0])
    ).to.be.revertedWith(
      'Not an operator of that token',
      'Attempt to claim as non-owner'
    )
    await expect(
      PORTAL.connect(moonCatOwner).claimReward(ownedMoonCats[1])
    ).to.be.revertedWith(
      'Not currently staked',
      'Attempt to claim unstaked token'
    )

    // Claim rewards should unlock token, even if no funds are given
    await expect(PORTAL.connect(moonCatOwner).claimReward(ownedMoonCats[0]))
      .to.emit(JUMPPORT, 'Unlock')
      .withArgs(
        PORTAL.address,
        moonCatOwner.address,
        ACCLIMATOR.address,
        ownedMoonCats[0]
      )

    expect(
      await JUMPPORT.isLocked(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.equal(false, 'MoonCat is now unlocked')
  })

  it('should properly transfer ERC20 credits', async function () {
    const accounts = await ethers.getSigners()
    let moonCatOwner = accounts[1]
    let ownedMoonCats = moonCatOwners[moonCatOwner.address]

    // Deposit all owned MoonCats into JumpPort
    await ACCLIMATOR.connect(moonCatOwner).setApprovalForAll(
      JUMPPORT.address,
      true
    )
    await JUMPPORT.connect(moonCatOwner)['deposit(address,uint256[])'](
      ACCLIMATOR.address,
      ownedMoonCats
    )

    // Configure Portal
    await PORTAL.setTargetTokens(DAI.address, ACCLIMATOR.address)
    await PORTAL.setRewardTiers([50, 100], [5, 500])
    await PORTAL.setEnabled(true)

    // Stake the token
    await PORTAL.connect(moonCatOwner).stakeToken(ownedMoonCats[0])

    await network.provider.send('hardhat_mine', ['0x32']) // Jump forward
    await expect(
      PORTAL.connect(moonCatOwner).claimReward(ownedMoonCats[0])
    ).to.be.revertedWith(
      'Dai/insufficient-balance',
      'Cannot pay out when empty'
    )

    let dai = DAI.connect(DAI_HOLDER)
    await dai.transfer(PORTAL.address, 1000)
    await expect(() =>
      PORTAL.connect(moonCatOwner).claimReward(ownedMoonCats[0])
    ).to.changeTokenBalance(dai, moonCatOwner, 5) // Received first-tier reward

    // Stake again
    await PORTAL.connect(moonCatOwner).stakeToken(ownedMoonCats[0])
    await expect(() =>
      PORTAL.connect(moonCatOwner).claimReward(ownedMoonCats[0])
    ).to.changeTokenBalance(dai, moonCatOwner, 0) // No additional reward

    // Stake again
    await PORTAL.connect(moonCatOwner).stakeToken(ownedMoonCats[0])
    await network.provider.send('hardhat_mine', ['0x32']) // Jump forward
    await expect(() =>
      PORTAL.connect(moonCatOwner).claimReward(ownedMoonCats[0])
    ).to.changeTokenBalance(dai, moonCatOwner, 5) // Received additional first-tier reward

    // Stake again
    await PORTAL.connect(moonCatOwner).stakeToken(ownedMoonCats[0])
    await network.provider.send('hardhat_mine', ['0x64']) // Jump forward
    await expect(() =>
      PORTAL.connect(moonCatOwner).claimReward(ownedMoonCats[0])
    ).to.changeTokenBalance(dai, moonCatOwner, 500) // Received second-tier reward
  })
})

async function updateMoonCatOwners(startIndex = 1000, endIndex = 1049) {
  let out = {}
  for (let i = startIndex; i <= endIndex; i++) {
    let owner = await ACCLIMATOR.ownerOf(i)
    if (typeof out[owner] == 'undefined') out[owner] = []
    out[owner].push(i)
  }
  return out
}
