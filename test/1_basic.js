require('dotenv').config()
const { network, ethers } = require('hardhat'),
  gasLedger = require('../lib/gasLedger'),
  {
    MoonCatAcclimator,
    JumpPort,
    disperseMoonCats,
    expectPermissionError,
  } = require('@mooncatrescue/contracts/moonCatUtils'),
  { expect } = require('chai')

const MOONCAT_REFERENCE_ADDRESS = '0x0B78C64bCE6d6d4447e58b09E53F3621f44A2a48'

let currentSnapshot = false
let moonCatOwners = {}

let ACCLIMATOR = false
let JUMPPORT = false

describe('MoonCat JumpPort basic logic', function () {
  before(async function () {
    const accounts = await ethers.getSigners()
    ACCLIMATOR = await MoonCatAcclimator
    JUMPPORT = await JumpPort

    // Set first Hardhat account as an admin of the JumpPort
    const jumpportAdmin = '0xdf2E60Af57C411F848B1eA12B10a404d194bce27'
    await accounts[5].sendTransaction({
      to: jumpportAdmin,
      value: ethers.utils.parseEther('0.3'),
    })
    await network.provider.request({
      method: 'hardhat_impersonateAccount',
      params: [jumpportAdmin],
    })
    let adminSigner = await ethers.provider.getSigner(jumpportAdmin)
    await JUMPPORT.connect(adminSigner).setAdmin(accounts[0].address, true)

    if (network.name == 'hardhat') {
      // Set up test accounts as the owners of several cats
      this.timeout(300000)
      await disperseMoonCats(1000, 1020)
    }
    moonCatOwners = await updateMoonCatOwners(1000, 1020)
  })
  after(function () {
    gasLedger.reportGasUsage()
    gasLedger.exportLedger(__filename)
  })

  beforeEach(async function () {
    currentSnapshot = await network.provider.request({
      method: 'evm_snapshot',
      params: [],
    })
  })
  afterEach(async function () {
    await network.provider.request({
      method: 'evm_revert',
      params: [currentSnapshot],
    })
  })

  it('should allow enumerating and updating admin roles', async function () {
    const accounts = await ethers.getSigners()

    // Starting state should have Account 0 as Admin and no others
    const ADMIN_ROLE = await JUMPPORT.ADMIN_ROLE()
    expect(await JUMPPORT.hasRole(ADMIN_ROLE, accounts[0].address)).to.equal(
      true,
      'Starts as Administrator'
    )
    expect(await JUMPPORT.hasRole(ADMIN_ROLE, accounts[1].address)).to.equal(
      false,
      'Starts as not Administrator'
    )

    // Others should not be allowed to grant admin role
    await expectPermissionError(
      JUMPPORT.connect(accounts[5]).setAdmin(accounts[1].address, true),
      ADMIN_ROLE,
      accounts[5].address
    )

    await expect(await JUMPPORT.setAdmin(accounts[1].address, true))
      .to.emit(JUMPPORT, 'RoleChange')
      .withArgs(ADMIN_ROLE, accounts[1].address, true, accounts[0].address)
    expect(await JUMPPORT.hasRole(ADMIN_ROLE, accounts[1].address)).to.equal(
      true,
      'Updated to be an Administrator'
    )

    // New Admin should be able to grant other admin roles
    await expect(
      await JUMPPORT.connect(accounts[1]).setAdmin(accounts[2].address, true)
    )
      .to.emit(JUMPPORT, 'RoleChange')
      .withArgs(ADMIN_ROLE, accounts[2].address, true, accounts[1].address)

    // Admin should be allowed to revoke own role
    await expect(
      await JUMPPORT.connect(accounts[1]).setAdmin(accounts[1].address, false)
    )
      .to.emit(JUMPPORT, 'RoleChange')
      .withArgs(ADMIN_ROLE, accounts[1].address, false, accounts[1].address)

    const PORTAL_ROLE = await JUMPPORT.PORTAL_ROLE()
    const PORTAL = accounts[5]
    // New admin should be able to grant Portal role
    await expect(
      await JUMPPORT.connect(accounts[2]).setPortalValidation(
        PORTAL.address,
        true
      )
    )
      .to.emit(JUMPPORT, 'RoleChange')
      .withArgs(PORTAL_ROLE, PORTAL.address, true, accounts[2].address)

    await expectPermissionError(
      JUMPPORT.connect(accounts[1]).setPortalValidation(PORTAL.address, true),
      ADMIN_ROLE,
      accounts[1].address
    )

    expect(await JUMPPORT.hasRole(ADMIN_ROLE, accounts[0].address)).to.equal(
      true,
      'Ends as Administrator'
    )
    expect(await JUMPPORT.hasRole(ADMIN_ROLE, accounts[1].address)).to.equal(
      false,
      'Ends as not Administrator'
    )
    expect(await JUMPPORT.hasRole(ADMIN_ROLE, accounts[2].address)).to.equal(
      true,
      'Ends as Administrator'
    )
    expect(await JUMPPORT.hasRole(PORTAL_ROLE, accounts[0].address)).to.equal(
      false,
      'Is not a Portal'
    )
    expect(await JUMPPORT.hasRole(PORTAL_ROLE, PORTAL.address)).to.equal(
      true,
      'Is a Portal'
    )
  })

  it('should allow admins to sign messages', async function () {
    const accounts = await ethers.getSigners()
    const messageHash =
      '0x034dc58f46f21d98834ab159a61709d956ddfd95bebccb9da821056794b32d52'
    const ADMIN_ROLE = await JUMPPORT.ADMIN_ROLE()

    expect(
      await JUMPPORT.connect(accounts[5]).isValidSignature(
        messageHash,
        '0x0102030405'
      )
    ).to.equal('0xffffffff', 'Message starts unsigned')
    await expectPermissionError(
      JUMPPORT.connect(accounts[5]).markMessageSigned(messageHash, 10000),
      ADMIN_ROLE,
      accounts[5].address
    )

    await JUMPPORT.markMessageSigned(messageHash, 100),
      expect(
        await JUMPPORT.connect(accounts[5]).isValidSignature(
          messageHash,
          '0x0102030405'
        )
      ).to.equal('0x1626ba7e', 'Message is now valid')

    // Jump forward
    await network.provider.send('hardhat_mine', [
      ethers.BigNumber.from('150').toHexString(),
    ])
    expect(
      await JUMPPORT.connect(accounts[5]).isValidSignature(
        messageHash,
        '0x0102030405'
      )
    ).to.equal('0xffffffff', 'Message has expired')
  })

  it('should report documentation properly', async function () {
    const accounts = await ethers.getSigners()
    expect(await JUMPPORT.DocumentationRepository()).to.equal(
      MOONCAT_REFERENCE_ADDRESS,
      'Repository public link'
    )
    const DOCUMENTATION = new ethers.Contract(
      MOONCAT_REFERENCE_ADDRESS,
      [
        'function owner () public view returns (address owner)',
        'function doc (address contractAddress) external view returns (string memory name, string memory description, string memory details)',
        'function setDoc (address contractAddress, string calldata name, string calldata description, string memory details) external',
      ],
      accounts[0]
    )

    let documentationOwnerAddress = await DOCUMENTATION.owner()
    DOC_OWNER = await ethers.provider.getSigner(documentationOwnerAddress)
    await hre.network.provider.request({
      method: 'hardhat_impersonateAccount',
      params: [documentationOwnerAddress],
    })

    // Set the documentation
    await DOCUMENTATION.connect(DOC_OWNER).setDoc(
      JUMPPORT.address,
      'foo',
      'bar',
      'baz'
    )

    // Retrive the documentation
    let rs = await JUMPPORT.doc()
    expect(rs.name).to.equal('foo')
    expect(rs.description).to.equal('bar')
    expect(rs.details).to.equal('baz')
  })

  it('should allow direct transfer of MoonCats to the JumpPort', async function () {
    const accounts = await ethers.getSigners()
    const moonCatOwner = accounts[1]
    const EVE = accounts[8]
    let ownedMoonCats = moonCatOwners[moonCatOwner.address]

    // Check starting state of asset as reported by JumpPort
    expect(
      await JUMPPORT.isDeposited(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.equal(false, 'MoonCat is not deposited')
    await expect(
      JUMPPORT.ownerOf(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.be.revertedWith(
      'Not currently deposited',
      'MoonCat starts as not deposited'
    )

    await testBalance(ACCLIMATOR.address, moonCatOwner.address, 0, 0)

    // Eve tries to pretend to deposit a MoonCat
    await JUMPPORT.connect(EVE).onERC721Received(
      ACCLIMATOR.address,
      EVE.address,
      1000,
      []
    )
    await testBalance(EVE.address, EVE.address, 1) // She has a balance of her own "token", which doesn't really do anything
    await expect(
      JUMPPORT.connect(EVE)['withdraw(address,uint256)'](EVE.address, 1000)
    ).to.be.reverted // Cannot withdraw from phony collection

    // Move an owned MoonCat to the JumpPort
    let tx = await ACCLIMATOR.connect(moonCatOwner)[
      'safeTransferFrom(address,address,uint256)'
    ](moonCatOwner.address, JUMPPORT.address, ownedMoonCats[0])
    await gasLedger.gasUsed(
      tx,
      this.test.title,
      'Deposit MoonCat with safeTransferFrom'
    )
    await expect(tx)
      .to.emit(JUMPPORT, 'Deposit')
      .withArgs(moonCatOwner.address, ACCLIMATOR.address, ownedMoonCats[0])
    let depositBlock = await ethers.provider.getBlock('latest')

    // Compare to just moving a MoonCat to a different wallet
    tx = await ACCLIMATOR.connect(accounts[5])[
      'safeTransferFrom(address,address,uint256)'
    ](
      accounts[5].address,
      accounts[6].address,
      moonCatOwners[accounts[5].address][0]
    )
    await gasLedger.gasUsed(
      tx,
      this.test.title,
      'MoonCat safeTransferFrom in general'
    )

    // Verify metadata recorded correctly for deposited asset
    expect(
      await JUMPPORT.ownerOf(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.equal(moonCatOwner.address, 'Recorded owner properly')
    await testBalance(ACCLIMATOR.address, moonCatOwner.address, 1)
    expect(await ACCLIMATOR.ownerOf(ownedMoonCats[0])).to.equal(
      JUMPPORT.address,
      'Recorded owner properly'
    )
    expect(
      await JUMPPORT.isLocked(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.equal(false, 'MoonCat is not locked initially')

    expect(
      await JUMPPORT.depositedSince(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.equal(depositBlock.number, 'Deposit block number recorded')

    // Verify non-owners cannot withdraw that token
    await expect(
      JUMPPORT.connect(EVE)['safeWithdraw(address,uint256,bytes)'](
        ACCLIMATOR.address,
        ownedMoonCats[0],
        []
      )
    ).to.be.revertedWith(
      'Not an operator of that token',
      'Withdraw as non-owner'
    )
    await expect(
      JUMPPORT.connect(EVE)['safeWithdraw(address,uint256)'](
        ACCLIMATOR.address,
        ownedMoonCats[0]
      )
    ).to.be.revertedWith(
      'Not an operator of that token',
      'Withdraw as non-owner'
    )
    await expect(
      JUMPPORT.connect(EVE)['safeWithdraw(address[],uint256[])'](
        [ACCLIMATOR.address],
        [ownedMoonCats[0]]
      )
    ).to.be.revertedWith(
      'Not an operator of that token',
      'Withdraw as non-owner'
    )
    await expect(
      JUMPPORT.connect(EVE)['withdraw(address,uint256)'](
        ACCLIMATOR.address,
        ownedMoonCats[0]
      )
    ).to.be.revertedWith(
      'Not an operator of that token',
      'Withdraw as non-owner'
    )
    await expect(
      JUMPPORT.connect(EVE)['withdraw(address[],uint256[])'](
        [ACCLIMATOR.address],
        [ownedMoonCats[0]]
      )
    ).to.be.revertedWith(
      'Not an operator of that token',
      'Withdraw as non-owner'
    )

    // Test safeWithdraw
    tx = await JUMPPORT.connect(moonCatOwner)['safeWithdraw(address,uint256)'](
      ACCLIMATOR.address,
      ownedMoonCats[0]
    )
    await gasLedger.gasUsed(
      tx,
      this.test.title,
      'Withdraw MoonCat with safeWithdraw'
    )
    let withdrawBlock = await ethers.provider.getBlock('latest')
    await expect(tx)
      .to.emit(JUMPPORT, 'Withdraw')
      .withArgs(
        moonCatOwner.address,
        ACCLIMATOR.address,
        ownedMoonCats[0],
        withdrawBlock.number - depositBlock.number
      )

    await expect(
      JUMPPORT.ownerOf(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.be.revertedWith(
      'Not currently deposited',
      'MoonCat no longer marked as deposited'
    )
    expect(await ACCLIMATOR.ownerOf(ownedMoonCats[0])).to.equal(
      moonCatOwner.address,
      'MoonCat transferred to new owner'
    )
    await testBalance(ACCLIMATOR.address, moonCatOwner.address, 0)

    // Move another owned MoonCat to the JumpPort
    tx = await ACCLIMATOR.connect(moonCatOwner)[
      'safeTransferFrom(address,address,uint256)'
    ](moonCatOwner.address, JUMPPORT.address, ownedMoonCats[1])
    await gasLedger.gasUsed(
      tx,
      this.test.title,
      'Deposit 2nd MoonCat with safeTransferFrom'
    )
    depositBlock = await ethers.provider.getBlock('latest')
    expect(
      await JUMPPORT.depositedSince(ACCLIMATOR.address, ownedMoonCats[1])
    ).to.equal(depositBlock.number, 'Deposit block number updated')

    // Test withdraw
    tx = await JUMPPORT.connect(moonCatOwner)['withdraw(address,uint256)'](
      ACCLIMATOR.address,
      ownedMoonCats[1]
    )
    await gasLedger.gasUsed(
      tx,
      this.test.title,
      'Withdraw 2nd MoonCat with withdraw'
    )
    withdrawBlock = await ethers.provider.getBlock('latest')
    await expect(tx)
      .to.emit(JUMPPORT, 'Withdraw')
      .withArgs(
        moonCatOwner.address,
        ACCLIMATOR.address,
        ownedMoonCats[1],
        withdrawBlock.number - depositBlock.number
      )

    await expect(
      JUMPPORT.ownerOf(ACCLIMATOR.address, ownedMoonCats[1])
    ).to.be.revertedWith(
      'Not currently deposited',
      'MoonCat no longer marked as deposited'
    )
    expect(await ACCLIMATOR.ownerOf(ownedMoonCats[1])).to.equal(
      moonCatOwner.address,
      'MoonCat transferred to new owner'
    )

    // Re-deposit same MoonCat
    tx = await ACCLIMATOR.connect(moonCatOwner)[
      'safeTransferFrom(address,address,uint256)'
    ](moonCatOwner.address, JUMPPORT.address, ownedMoonCats[1])
    await gasLedger.gasUsed(
      tx,
      this.test.title,
      'Re-Deposit MoonCat with safeTransferFrom'
    )
    block = await ethers.provider.getBlock('latest')
    expect(
      await JUMPPORT.depositedSince(ACCLIMATOR.address, ownedMoonCats[1])
    ).to.equal(block.number, 'Deposit block number updated')
  })

  it('should allow approving and depositing MoonCats', async function () {
    const accounts = await ethers.getSigners()
    const moonCatOwner = accounts[1]
    const EVE = accounts[8]
    let ownedMoonCats = moonCatOwners[moonCatOwner.address]

    await ACCLIMATOR.connect(moonCatOwner).approve(
      JUMPPORT.address,
      ownedMoonCats[0]
    ) // Mark JumpPort as an approved operator of a MoonCat

    await expect(
      JUMPPORT.connect(EVE)['deposit(address,uint256)'](
        ACCLIMATOR.address,
        ownedMoonCats[0]
      )
    ).to.be.revertedWith(
      'ERC721: transfer of token that is not own',
      'Deposit initiaited by non-owner'
    )

    let tx = await JUMPPORT.connect(moonCatOwner)['deposit(address,uint256)'](
      ACCLIMATOR.address,
      ownedMoonCats[0]
    )
    await gasLedger.gasUsed(
      tx,
      this.test.title,
      'Deposit MoonCat with deposit function'
    )
    await expect(tx)
      .to.emit(JUMPPORT, 'Deposit')
      .withArgs(moonCatOwner.address, ACCLIMATOR.address, ownedMoonCats[0])

    // Verify metadata recorded correctly for deposited asset
    expect(
      await JUMPPORT.ownerOf(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.equal(moonCatOwner.address, 'Recorded owner properly')
    await testBalance(ACCLIMATOR.address, moonCatOwner.address, 1)
    expect(await ACCLIMATOR.ownerOf(ownedMoonCats[0])).to.equal(
      JUMPPORT.address,
      'Recorded owner properly'
    )

    // Test withdraw
    await JUMPPORT.connect(moonCatOwner)['withdraw(address,uint256)'](
      ACCLIMATOR.address,
      ownedMoonCats[0]
    )
    await expect(
      JUMPPORT.ownerOf(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.be.revertedWith(
      'Not currently deposited',
      'MoonCat no longer marked as deposited'
    )
    expect(await ACCLIMATOR.ownerOf(ownedMoonCats[0])).to.equal(
      moonCatOwner.address,
      'MoonCat transferred to new owner'
    )
    await testBalance(ACCLIMATOR.address, moonCatOwner.address, 0)

    // Test deposit with approval for all
    await ACCLIMATOR.connect(moonCatOwner).setApprovalForAll(
      JUMPPORT.address,
      true
    )
    tx = await JUMPPORT.connect(moonCatOwner)['deposit(address,uint256)'](
      ACCLIMATOR.address,
      ownedMoonCats[1]
    )
    await gasLedger.gasUsed(
      tx,
      this.test.title,
      'Deposit 2nd MoonCat with deposit function'
    )

    expect(
      await JUMPPORT.ownerOf(ACCLIMATOR.address, ownedMoonCats[1])
    ).to.equal(moonCatOwner.address, 'Recorded owner properly')
    expect(await ACCLIMATOR.ownerOf(ownedMoonCats[1])).to.equal(
      JUMPPORT.address,
      'Recorded owner properly'
    )

    // Test withdraw
    await JUMPPORT.connect(moonCatOwner)['withdraw(address,uint256)'](
      ACCLIMATOR.address,
      ownedMoonCats[1]
    )
    await expect(
      JUMPPORT.ownerOf(ACCLIMATOR.address, ownedMoonCats[1])
    ).to.be.revertedWith(
      'Not currently deposited',
      'MoonCat no longer marked as deposited'
    )
    expect(await ACCLIMATOR.ownerOf(ownedMoonCats[1])).to.equal(
      moonCatOwner.address,
      'MoonCat transferred to new owner'
    )
  })

  it('should allow approving and depositing multiple MoonCats at once', async function () {
    const accounts = await ethers.getSigners()
    const moonCatOwner = accounts[1]
    const EVE = accounts[8]
    let ownedMoonCats = moonCatOwners[moonCatOwner.address]
    let moonCatTokenAddresses = ownedMoonCats.map((mc) => ACCLIMATOR.address) // Create an array of repetitions of the Acclimated MoonCat address

    await ACCLIMATOR.connect(moonCatOwner).setApprovalForAll(
      JUMPPORT.address,
      true
    ) // Mark JumpPort contract as approved for all the owner's MoonCats
    await expect(
      JUMPPORT.connect(EVE)['deposit(address[],uint256[])'](
        moonCatTokenAddresses,
        ownedMoonCats
      )
    ).to.be.revertedWith(
      'ERC721: transfer of token that is not own',
      'Deposit initiaited by non-owner'
    )

    let tx = await JUMPPORT.connect(moonCatOwner)[
      'deposit(address[],uint256[])'
    ](moonCatTokenAddresses, ownedMoonCats)
    await gasLedger.gasUsed(
      tx,
      this.test.title,
      `Deposit ${ownedMoonCats.length} MoonCats at once with deposit function`
    )
    await expect(tx)
      .to.emit(JUMPPORT, 'Deposit')
      .withArgs(moonCatOwner.address, ACCLIMATOR.address, ownedMoonCats[0])
    await expect(tx)
      .to.emit(JUMPPORT, 'Deposit')
      .withArgs(moonCatOwner.address, ACCLIMATOR.address, ownedMoonCats[1])

    // Verify metadata recorded correctly for deposited assets
    expect(
      await JUMPPORT.ownerOf(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.equal(moonCatOwner.address, 'Recorded owner properly')
    expect(
      await JUMPPORT.ownerOf(ACCLIMATOR.address, ownedMoonCats[1])
    ).to.equal(moonCatOwner.address, 'Recorded owner properly')
    expect(await ACCLIMATOR.ownerOf(ownedMoonCats[0])).to.equal(
      JUMPPORT.address,
      'Recorded owner properly'
    )
    expect(await ACCLIMATOR.ownerOf(ownedMoonCats[1])).to.equal(
      JUMPPORT.address,
      'Recorded owner properly'
    )
  })

  it('should be able to pause deposits', async function () {
    const accounts = await ethers.getSigners()
    expect(await JUMPPORT.depositPaused()).to.equal(
      false,
      'Deposits start unpaused'
    )

    // Put all owned MoonCats for a few users into JumpPort
    await depositAllOwned(accounts[1])
    await depositAllOwned(accounts[2])

    const ADMIN_ROLE = await JUMPPORT.ADMIN_ROLE()
    await expectPermissionError(
      JUMPPORT.connect(accounts[6]).setPaused(true),
      ADMIN_ROLE,
      accounts[6].address
    )

    // Pause the contract
    await JUMPPORT.setPaused(true)
    expect(await JUMPPORT.depositPaused()).to.equal(true, 'Deposits paused')

    await ACCLIMATOR.connect(accounts[3]).setApprovalForAll(
      JUMPPORT.address,
      true
    )
    await expect(
      JUMPPORT.connect(accounts[3])['deposit(address,uint256[])'](
        ACCLIMATOR.address,
        moonCatOwners[accounts[3].address]
      )
    ).to.be.revertedWith('Paused', 'Try to deposit while paused')
  })

  it('should be able to enumerate owned tokens by owner address', async function () {
    const { accounts, moonCatOwner, ownedMoonCats } = await setup()

    // Put more MoonCats into JumpPort
    await depositAllOwned(accounts[2])

    await testBalance(
      ACCLIMATOR.address,
      accounts[1].address,
      moonCatOwners[accounts[1].address].length
    )
    await testBalance(
      ACCLIMATOR.address,
      accounts[2].address,
      moonCatOwners[accounts[2].address].length
    )
    await testBalance(ACCLIMATOR.address, accounts[3].address, 0)

    await depositAllOwned(accounts[3])

    await testBalance(
      ACCLIMATOR.address,
      accounts[3].address,
      moonCatOwners[accounts[3].address].length
    )
    await testBalance(
      [ACCLIMATOR.address],
      accounts[3].address,
      moonCatOwners[accounts[3].address].length
    )
    await testBalance(
      [ACCLIMATOR.address, JUMPPORT.address, accounts[5].address],
      accounts[3].address,
      moonCatOwners[accounts[3].address].length
    )

    // Estimate gas without actually executing
    let gasEstimate = await JUMPPORT.connect(
      accounts[5]
    ).estimateGas.ownedTokens(
      ACCLIMATOR.address,
      moonCatOwner.address,
      500,
      1500
    )
    gasLedger.gasEstimate(
      gasEstimate.toNumber(),
      this.test.title,
      'Owner enumeration estimate across 1,000 token range'
    )

    let rs = await JUMPPORT.connect(accounts[5]).ownedTokens(
      ACCLIMATOR.address,
      moonCatOwner.address,
      1000,
      1020
    )
    expect(rs.map((n) => n.toNumber())).to.have.same.members(
      ownedMoonCats,
      'Find all owned MoonCats'
    )
  })

  it('should be able to withdraw multiple tokens at once', async function () {
    const { moonCatOwner, EVE, ownedMoonCats } = await setup()

    // Withdraw multiple should not allow non-owners to do so
    let collectionAddresses = ownedMoonCats.map((tokenId) => ACCLIMATOR.address)
    await expect(
      JUMPPORT.connect(EVE)['safeWithdraw(address[],uint256[])'](
        collectionAddresses,
        ownedMoonCats
      )
    ).to.be.revertedWith(
      'Not an operator of that token',
      'Withdraw as non-owner'
    )

    // Withdraw all the MoonCats with safeWithdraw
    let tx = await JUMPPORT.connect(moonCatOwner)[
      'safeWithdraw(address[],uint256[])'
    ](collectionAddresses, ownedMoonCats)
    await gasLedger.gasUsed(
      tx,
      this.test.title,
      `Withdraw ${ownedMoonCats.length} MoonCats with safeWithdraw`
    )

    expect(await ACCLIMATOR.ownerOf(ownedMoonCats[0])).to.equal(
      moonCatOwner.address,
      'Ownership updated'
    )
    expect(await ACCLIMATOR.ownerOf(ownedMoonCats[1])).to.equal(
      moonCatOwner.address,
      'Ownership updated'
    )
  })

  it('should be able to enumerate token owners', async function () {
    const { accounts } = await setup()

    // Put more MoonCats into JumpPort
    await depositAllOwned(accounts[2])
    await depositAllOwned(accounts[3])

    let gasEstimate = await JUMPPORT.connect(accounts[5]).estimateGas.ownersOf(
      ACCLIMATOR.address,
      500,
      1500
    )
    gasLedger.gasEstimate(
      gasEstimate.toNumber(),
      this.test.title,
      'Token owners enumeration estimate across 1,000 token range'
    )

    let rs = await JUMPPORT.connect(accounts[5]).ownersOf(
      ACCLIMATOR.address,
      1000,
      1020
    )

    // Construct expected result
    let expected = []
    for (let i = 0; i <= 20; i++) {
      expected[i] = '0x0000000000000000000000000000000000000000'
    }
    expected[1] = accounts[1].address
    expected[2] = accounts[2].address
    expected[3] = accounts[3].address
    expected[11] = accounts[1].address
    expected[12] = accounts[2].address
    expected[13] = accounts[3].address

    expect(rs).to.have.same.members(expected, 'Find all owned MoonCats')
  })

  it('should allow setting copilots', async function () {
    const { accounts, moonCatOwner, ownedMoonCats } = await setup()

    // Test address to be used as Copilot
    const copilot = accounts[3]

    await expect(
      JUMPPORT.connect(copilot)['safeWithdraw(address,uint256)'](
        ACCLIMATOR.address,
        ownedMoonCats[0]
      )
    ).to.be.revertedWith(
      'Not an operator of that token',
      'Attempt to withdraw before authorized'
    )

    // Mark Copilot as such, for a single MoonCat
    await expect(
      JUMPPORT.connect(moonCatOwner).setCopilot(
        copilot.address,
        ACCLIMATOR.address,
        ownedMoonCats[0]
      )
    )
      .to.emit(JUMPPORT, 'Approval')
      .withArgs(
        moonCatOwner.address,
        copilot.address,
        ACCLIMATOR.address,
        ownedMoonCats[0]
      )

    // Verify Copilot metadata was recorded
    expect(
      await JUMPPORT.getApproved(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.equal(copilot.address, 'Token approval recorded')

    // Attempt to withdraw as the Copilot
    await expect(
      JUMPPORT.connect(copilot)['safeWithdraw(address,uint256)'](
        ACCLIMATOR.address,
        ownedMoonCats[0]
      )
    )
      .to.emit(JUMPPORT, 'Withdraw')
      .withArgs(
        moonCatOwner.address, // Even though the Copilot triggered it, event should be for Owner
        ACCLIMATOR.address,
        ownedMoonCats[0],
        3
      )

    // Send MoonCat back
    await ACCLIMATOR.connect(moonCatOwner)[
      'safeTransferFrom(address,address,uint256)'
    ](moonCatOwner.address, JUMPPORT.address, ownedMoonCats[0])

    // Verify Copilot status is reset after withdrawing
    expect(
      await JUMPPORT.getApproved(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.equal(ethers.constants.AddressZero, 'Token approval revoked')
    await expect(
      JUMPPORT.connect(copilot)['safeWithdraw(address,uint256)'](
        ACCLIMATOR.address,
        ownedMoonCats[0]
      )
    ).to.be.revertedWith(
      'Not an operator of that token',
      'Authorization should be revoked'
    )

    // Authorize for all
    expect(
      await JUMPPORT.isApprovedForAll(moonCatOwner.address, copilot.address)
    ).to.equal(false, 'Metadata starts as false')
    await expect(
      JUMPPORT.connect(moonCatOwner).setCopilotForAll(copilot.address, true)
    )
      .to.emit(JUMPPORT, 'ApprovalForAll')
      .withArgs(moonCatOwner.address, copilot.address, true)
    expect(
      await JUMPPORT.isApprovedForAll(moonCatOwner.address, copilot.address)
    ).to.equal(true, 'Metadata marked appropriately')

    // Verify can withdraw all the MoonCats owned by that address
    await JUMPPORT.connect(copilot)['safeWithdraw(address,uint256)'](
      ACCLIMATOR.address,
      ownedMoonCats[0]
    )
    await expect(
      JUMPPORT.connect(copilot)['withdraw(address,uint256)'](
        ACCLIMATOR.address,
        ownedMoonCats[1]
      )
    )
      .to.emit(JUMPPORT, 'Withdraw')
      .withArgs(
        moonCatOwner.address, // Even though the Copilot triggered it, event should be for Owner
        ACCLIMATOR.address,
        ownedMoonCats[1],
        8
      )
  })

  it('should allow revoking copilots', async function () {
    const { accounts, moonCatOwner, ownedMoonCats } = await setup()

    // Test address to be used as Copilot
    const copilot = accounts[3]

    // Mark Copilot as such, for a single MoonCat
    await JUMPPORT.connect(moonCatOwner).setCopilot(
      copilot.address,
      ACCLIMATOR.address,
      ownedMoonCats[0]
    )

    // Verify Copilot metadata was recorded
    expect(
      await JUMPPORT.getApproved(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.equal(copilot.address, 'Token approval recorded')

    // Revoke Copilot status
    await expect(
      JUMPPORT.connect(moonCatOwner).setCopilot(
        ethers.constants.AddressZero,
        ACCLIMATOR.address,
        ownedMoonCats[0]
      )
    )
      .to.emit(JUMPPORT, 'Approval')
      .withArgs(
        moonCatOwner.address,
        ethers.constants.AddressZero,
        ACCLIMATOR.address,
        ownedMoonCats[0]
      )

    // Attempt to withdraw as the revoked Copilot
    await expect(
      JUMPPORT.connect(copilot)['safeWithdraw(address,uint256)'](
        ACCLIMATOR.address,
        ownedMoonCats[0]
      )
    ).to.be.revertedWith(
      'Not an operator of that token',
      'Attempt to withdraw after being revoked'
    )

    // Authorize for all
    await JUMPPORT.connect(moonCatOwner).setCopilotForAll(copilot.address, true)
    expect(
      await JUMPPORT.isApprovedForAll(moonCatOwner.address, copilot.address)
    ).to.equal(true, 'Metadata marked appropriately')

    // Revoke Copilot status
    await expect(
      JUMPPORT.connect(moonCatOwner).setCopilotForAll(copilot.address, false)
    )
      .to.emit(JUMPPORT, 'ApprovalForAll')
      .withArgs(moonCatOwner.address, copilot.address, false)
    expect(
      await JUMPPORT.isApprovedForAll(moonCatOwner.address, copilot.address)
    ).to.equal(false, 'Metadata marked appropriately')

    // Attempt to withdraw as the revoked Copilot
    await expect(
      JUMPPORT.connect(copilot)['safeWithdraw(address,uint256)'](
        ACCLIMATOR.address,
        ownedMoonCats[0]
      )
    ).to.be.revertedWith(
      'Not an operator of that token',
      'Attempt to withdraw after being revoked'
    )
  })

  it('should allow portals to lock tokens', async function () {
    const accounts = await ethers.getSigners()
    let moonCatOwner = accounts[1]
    let ownedMoonCats = moonCatOwners[moonCatOwner.address]

    // Put one MoonCat into JumpPort
    await ACCLIMATOR.connect(moonCatOwner)[
      'safeTransferFrom(address,address,uint256)'
    ](moonCatOwner.address, JUMPPORT.address, ownedMoonCats[0])

    // Verify locked metadata starting state
    expect(
      await JUMPPORT.isLocked(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.equal(false, 'MoonCat starts unlocked')
    await expect(
      JUMPPORT.isLocked(ACCLIMATOR.address, ownedMoonCats[1])
    ).to.be.revertedWith(
      'Not currently deposited',
      'No data for undeposited MoonCat'
    )

    // Set up a manual address as a Portal for testing
    const PORTAL_ROLE = await JUMPPORT.PORTAL_ROLE()
    const PORTAL = accounts[5]
    await expectPermissionError(
      JUMPPORT.connect(PORTAL).lockToken(ACCLIMATOR.address, ownedMoonCats[0]),
      PORTAL_ROLE,
      PORTAL.address
    )

    // Once marked as a valid Portal, can lock tokens
    await JUMPPORT.setPortalValidation(PORTAL.address, true)
    await expect(
      JUMPPORT.connect(PORTAL).lockToken(ACCLIMATOR.address, ownedMoonCats[0])
    )
      .to.emit(JUMPPORT, 'Lock')
      .withArgs(
        PORTAL.address,
        moonCatOwner.address,
        ACCLIMATOR.address,
        ownedMoonCats[0]
      )

    // Verify metadata that token is locked in JumpPort now
    expect(
      await JUMPPORT.isLocked(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.equal(true, 'MoonCat is now locked')
    await expect(
      JUMPPORT.connect(moonCatOwner)['safeWithdraw(address,uint256)'](
        ACCLIMATOR.address,
        ownedMoonCats[0]
      )
    ).to.be.revertedWith('Token is locked', 'Attempt to withdraw locked token')

    // Verify Portal can unlock the token
    await expect(
      JUMPPORT.connect(PORTAL).unlockToken(ACCLIMATOR.address, ownedMoonCats[0])
    )
      .to.emit(JUMPPORT, 'Unlock')
      .withArgs(
        PORTAL.address,
        moonCatOwner.address,
        ACCLIMATOR.address,
        ownedMoonCats[0]
      )
    expect(
      await JUMPPORT.isLocked(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.equal(false, 'MoonCat is now unlocked')

    // Attempt to withdraw, now that it's unlocked
    await JUMPPORT.connect(moonCatOwner)['safeWithdraw(address,uint256)'](
      ACCLIMATOR.address,
      ownedMoonCats[0]
    )
    expect(
      await JUMPPORT.isDeposited(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.equal(false, 'MoonCat successfully withdrawn')
    expect(await ACCLIMATOR.ownerOf(ownedMoonCats[0])).to.equal(
      moonCatOwner.address,
      'Transfer back to owner completed'
    )
  })

  it('should properly track multiple locks on tokens', async function () {
    const { accounts, ownedMoonCats } = await setup()

    // Use three manual wallets to emulate three Portals
    const portal1 = accounts[5],
      portal2 = accounts[6],
      portal3 = accounts[7]

    await JUMPPORT.setPortalValidation(portal1.address, true)
    await JUMPPORT.setPortalValidation(portal2.address, true)
    await JUMPPORT.setPortalValidation(portal3.address, true)

    // Locks applied in ascending order for first MoonCat
    let tx = await JUMPPORT.connect(portal1).lockToken(
      ACCLIMATOR.address,
      ownedMoonCats[0]
    )
    await gasLedger.gasUsed(tx, this.test.title, 'Lock 1 on MoonCat')

    tx = await JUMPPORT.connect(portal2).lockToken(
      ACCLIMATOR.address,
      ownedMoonCats[0]
    )
    await gasLedger.gasUsed(tx, this.test.title, 'Lock 2 on MoonCat')

    tx = await JUMPPORT.connect(portal3).lockToken(
      ACCLIMATOR.address,
      ownedMoonCats[0]
    )
    await gasLedger.gasUsed(tx, this.test.title, 'Lock 3 on MoonCat')

    expect(
      await JUMPPORT.isLocked(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.equal(true, 'MoonCat is now locked')
    expect(
      await JUMPPORT.lockedBy(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.have.members(
      [portal1.address, portal2.address, portal3.address],
      'Portal locks enumerated correctly'
    )

    // Locks applied in reverse order for second MoonCat
    await JUMPPORT.connect(portal3).lockToken(
      ACCLIMATOR.address,
      ownedMoonCats[1]
    )
    await JUMPPORT.connect(portal2).lockToken(
      ACCLIMATOR.address,
      ownedMoonCats[1]
    )
    await JUMPPORT.connect(portal1).lockToken(
      ACCLIMATOR.address,
      ownedMoonCats[1]
    )
    expect(
      await JUMPPORT.isLocked(ACCLIMATOR.address, ownedMoonCats[1])
    ).to.equal(true, 'MoonCat is now locked')
    expect(
      await JUMPPORT.lockedBy(ACCLIMATOR.address, ownedMoonCats[1])
    ).to.have.members(
      [portal1.address, portal2.address, portal3.address],
      'Portal locks enumerated correctly'
    )

    // Undo locks in order for first MoonCat
    tx = await JUMPPORT.connect(portal1).unlockToken(
      ACCLIMATOR.address,
      ownedMoonCats[0]
    )
    await gasLedger.gasUsed(tx, this.test.title, 'Unlock 1 on MoonCat')
    expect(
      await JUMPPORT.isLocked(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.equal(true, 'MoonCat is still locked')
    expect(
      await JUMPPORT.lockedBy(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.have.members(
      [portal2.address, portal3.address],
      'Portal locks enumerated correctly'
    )

    tx = await JUMPPORT.connect(portal2).unlockToken(
      ACCLIMATOR.address,
      ownedMoonCats[0]
    )
    await gasLedger.gasUsed(tx, this.test.title, 'Unlock 2 on MoonCat')
    expect(
      await JUMPPORT.isLocked(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.equal(true, 'MoonCat is still locked')

    tx = await JUMPPORT.connect(portal3).unlockToken(
      ACCLIMATOR.address,
      ownedMoonCats[0]
    )
    await gasLedger.gasUsed(tx, this.test.title, 'Unlock 3 on MoonCat')
    expect(
      await JUMPPORT.isLocked(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.equal(false, 'MoonCat is now unlocked')

    // Reapply partially
    await JUMPPORT.connect(portal2).lockToken(
      ACCLIMATOR.address,
      ownedMoonCats[0]
    )
    await JUMPPORT.connect(portal3).lockToken(
      ACCLIMATOR.address,
      ownedMoonCats[0]
    )
    expect(
      await JUMPPORT.isLocked(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.equal(true, 'MoonCat is now locked')

    await JUMPPORT.connect(portal2).unlockToken(
      ACCLIMATOR.address,
      ownedMoonCats[0]
    )
    expect(
      await JUMPPORT.isLocked(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.equal(true, 'MoonCat is still locked')
    await JUMPPORT.connect(portal1).unlockToken(
      ACCLIMATOR.address,
      ownedMoonCats[0]
    ) // Was not locked; should have no effect
    expect(
      await JUMPPORT.isLocked(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.equal(true, 'MoonCat is still locked')
    await JUMPPORT.connect(portal3).unlockToken(
      ACCLIMATOR.address,
      ownedMoonCats[0]
    )
    expect(
      await JUMPPORT.isLocked(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.equal(false, 'MoonCat is now unlocked')

    // Undo locks in order for second MoonCat
    await JUMPPORT.connect(portal1).unlockToken(
      ACCLIMATOR.address,
      ownedMoonCats[1]
    )
    expect(
      await JUMPPORT.isLocked(ACCLIMATOR.address, ownedMoonCats[1])
    ).to.equal(true, 'MoonCat is still locked')
    await JUMPPORT.connect(portal2).unlockToken(
      ACCLIMATOR.address,
      ownedMoonCats[1]
    )
    expect(
      await JUMPPORT.isLocked(ACCLIMATOR.address, ownedMoonCats[1])
    ).to.equal(true, 'MoonCat is still locked')
    await JUMPPORT.connect(portal3).unlockToken(
      ACCLIMATOR.address,
      ownedMoonCats[1]
    )
    expect(
      await JUMPPORT.isLocked(ACCLIMATOR.address, ownedMoonCats[1])
    ).to.equal(false, 'MoonCat is now unlocked')
  })

  it('should allow re-locking from the same portal', async function () {
    const { accounts, ownedMoonCats } = await setup()

    // Use manual wallets to emulate Portals
    const portal1 = accounts[5],
      portal2 = accounts[6],
      portal3 = accounts[7],
      portal4 = accounts[8],
      portal5 = accounts[9]

    await JUMPPORT.setPortalValidation(portal1.address, true)
    await JUMPPORT.setPortalValidation(portal2.address, true)
    await JUMPPORT.setPortalValidation(portal3.address, true)
    await JUMPPORT.setPortalValidation(portal4.address, true)
    await JUMPPORT.setPortalValidation(portal5.address, true)

    // Locks applied in ascending order
    await JUMPPORT.connect(portal1).lockToken(
      ACCLIMATOR.address,
      ownedMoonCats[0]
    )
    await JUMPPORT.connect(portal2).lockToken(
      ACCLIMATOR.address,
      ownedMoonCats[0]
    )
    await JUMPPORT.connect(portal3).lockToken(
      ACCLIMATOR.address,
      ownedMoonCats[0]
    )

    expect(
      await JUMPPORT.isLocked(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.equal(true, 'MoonCat is now locked')
    expect(
      await JUMPPORT.lockedBy(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.have.members(
      [portal1.address, portal2.address, portal3.address],
      'Portal locks enumerated correctly'
    )

    // Unlock the middle lock
    await JUMPPORT.connect(portal2).unlockToken(
      ACCLIMATOR.address,
      ownedMoonCats[0]
    )
    expect(
      await JUMPPORT.isLocked(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.equal(true, 'MoonCat is still locked')
    expect(
      await JUMPPORT.lockedBy(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.have.members(
      [portal1.address, portal3.address],
      'Portal locks enumerated correctly'
    )

    // Re-lock the middle lock
    await JUMPPORT.connect(portal2).lockToken(
      ACCLIMATOR.address,
      ownedMoonCats[0]
    )
    expect(
      await JUMPPORT.lockedBy(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.have.members(
      [portal1.address, portal2.address, portal3.address],
      'Portal locks enumerated correctly'
    )

    // Unlock and add new lock
    await JUMPPORT.connect(portal1).unlockToken(
      ACCLIMATOR.address,
      ownedMoonCats[0]
    )
    await JUMPPORT.connect(portal4).lockToken(
      ACCLIMATOR.address,
      ownedMoonCats[0]
    )
    expect(
      await JUMPPORT.lockedBy(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.have.members(
      [portal2.address, portal3.address, portal4.address],
      'Portal locks enumerated correctly'
    )

    // Unlock and add new lock
    await JUMPPORT.connect(portal2).unlockToken(
      ACCLIMATOR.address,
      ownedMoonCats[0]
    )
    await JUMPPORT.connect(portal1).lockToken(
      ACCLIMATOR.address,
      ownedMoonCats[0]
    )
    await JUMPPORT.connect(portal5).lockToken(
      ACCLIMATOR.address,
      ownedMoonCats[0]
    )
    expect(
      await JUMPPORT.lockedBy(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.have.members(
      [portal1.address, portal3.address, portal4.address, portal5.address],
      'Portal locks enumerated correctly'
    )

    // Unlock nearly all
    await JUMPPORT.connect(portal1).unlockToken(
      ACCLIMATOR.address,
      ownedMoonCats[0]
    )
    await JUMPPORT.connect(portal2).unlockToken(
      ACCLIMATOR.address,
      ownedMoonCats[0]
    ) // Doesn't really do anything; already unlocked
    await JUMPPORT.connect(portal3).unlockToken(
      ACCLIMATOR.address,
      ownedMoonCats[0]
    )
    await JUMPPORT.connect(portal4).unlockToken(
      ACCLIMATOR.address,
      ownedMoonCats[0]
    )
    expect(
      await JUMPPORT.lockedBy(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.have.members([portal5.address], 'Portal locks enumerated correctly')

    // Re-lock one with a gap
    await JUMPPORT.connect(portal2).lockToken(
      ACCLIMATOR.address,
      ownedMoonCats[0]
    )
    expect(
      await JUMPPORT.lockedBy(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.have.members(
      [portal2.address, portal5.address],
      'Portal locks enumerated correctly'
    )

    // Unlock all
    await JUMPPORT.connect(portal2).unlockToken(
      ACCLIMATOR.address,
      ownedMoonCats[0]
    )
    await JUMPPORT.connect(portal5).unlockToken(
      ACCLIMATOR.address,
      ownedMoonCats[0]
    )
    expect(
      await JUMPPORT.isLocked(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.equal(false, 'MoonCat is now unlocked')
    let rs = await JUMPPORT.lockedBy(ACCLIMATOR.address, ownedMoonCats[0])
    expect(rs.length).to.equal(0, 'No locks held')
  })

  it('should allow contract owner override of locks on tokens', async function () {
    const { accounts, moonCatOwner, ownedMoonCats } = await setup()

    // Use manual wallets to emulate a few Portals
    let portal1 = accounts[5]
    let portal2 = accounts[6]

    await JUMPPORT.setPortalValidation(portal1.address, true)
    await JUMPPORT.setPortalValidation(portal2.address, true)

    // Lock the tokens in place
    await JUMPPORT.connect(portal1).lockToken(
      ACCLIMATOR.address,
      ownedMoonCats[0]
    )
    await JUMPPORT.connect(portal2).lockToken(
      ACCLIMATOR.address,
      ownedMoonCats[0]
    )
    await JUMPPORT.connect(portal2).lockToken(
      ACCLIMATOR.address,
      ownedMoonCats[1]
    )

    const ADMIN_ROLE = await JUMPPORT.ADMIN_ROLE()
    await expectPermissionError(
      JUMPPORT.connect(moonCatOwner).setAdminLockOverride(
        portal2.address,
        true
      ),
      ADMIN_ROLE,
      moonCatOwner.address
    )

    // Set one of the portals to not be able to lock the tokens
    await JUMPPORT.setAdminLockOverride(portal2.address, true)

    // The MoonCat that is locked by both portals should still be locked
    expect(
      await JUMPPORT.isLocked(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.equal(true, 'MoonCat is still locked')
    await expect(
      JUMPPORT.connect(moonCatOwner)['safeWithdraw(address,uint256)'](
        ACCLIMATOR.address,
        ownedMoonCats[0]
      )
    ).to.be.revertedWith('Token is locked', 'Attempt to withdraw locked token')

    // The MoonCat that is locked by just the overriden portal should now be unlocked
    expect(
      await JUMPPORT.isLocked(ACCLIMATOR.address, ownedMoonCats[1])
    ).to.equal(false, 'MoonCat is now unlocked')
    await JUMPPORT.connect(moonCatOwner)['safeWithdraw(address,uint256)'](
      ACCLIMATOR.address,
      ownedMoonCats[1]
    )
    expect(
      await JUMPPORT.isDeposited(ACCLIMATOR.address, ownedMoonCats[1])
    ).to.equal(false, 'MoonCat successfully withdrawn')
    expect(await ACCLIMATOR.ownerOf(ownedMoonCats[1])).to.equal(
      moonCatOwner.address,
      'Transfer back to owner completed'
    )

    // Unlock the token individually from the other portal
    await JUMPPORT.connect(portal1).unlockToken(
      ACCLIMATOR.address,
      ownedMoonCats[0]
    )

    // The MoonCat locked by both portals should now be unlocked
    expect(
      await JUMPPORT.isLocked(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.equal(false, 'MoonCat is now unlocked')

    // In this scenario, the portal has now fixed itself; re-enable locks for it
    await JUMPPORT.setAdminLockOverride(portal2.address, false)

    // The MoonCat that was withdrawn should not be "locked" since it's not deposited any more
    await expect(
      JUMPPORT.isLocked(ACCLIMATOR.address, ownedMoonCats[1])
    ).to.be.revertedWith('Not currently deposited', 'MoonCat is not present')

    // Put MoonCat back into JumpPort (why would the owner do this...?)
    await JUMPPORT.connect(moonCatOwner)['deposit(address,uint256)'](
      ACCLIMATOR.address,
      ownedMoonCats[1]
    )

    // Will now be locked, as that individual lock was not disengaged
    expect(
      await JUMPPORT.isLocked(ACCLIMATOR.address, ownedMoonCats[1])
    ).to.equal(true, 'MoonCat is now locked')
  })

  it('should allow portals to override their own locks on tokens', async function () {
    const { accounts, moonCatOwner, ownedMoonCats } = await setup()

    // Use manual wallets to emulate a few Portals
    let portal1 = accounts[5]
    let portal2 = accounts[6]

    await JUMPPORT.setPortalValidation(portal1.address, true)
    await JUMPPORT.setPortalValidation(portal2.address, true)

    // Lock the tokens in place
    await JUMPPORT.connect(portal1).lockToken(
      ACCLIMATOR.address,
      ownedMoonCats[0]
    )
    await JUMPPORT.connect(portal2).lockToken(
      ACCLIMATOR.address,
      ownedMoonCats[0]
    )
    await JUMPPORT.connect(portal2).lockToken(
      ACCLIMATOR.address,
      ownedMoonCats[1]
    )

    const PORTAL_ROLE = await JUMPPORT.PORTAL_ROLE()
    await expectPermissionError(
      JUMPPORT.connect(moonCatOwner).unlockAllTokens(true),
      PORTAL_ROLE,
      moonCatOwner.address
    )

    // One of the portals signals all of its locks should be invalidated
    await JUMPPORT.connect(portal2).unlockAllTokens(true)

    // The MoonCat that is locked by both portals should still be locked
    expect(
      await JUMPPORT.isLocked(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.equal(true, 'MoonCat is still locked')
    await expect(
      JUMPPORT.connect(moonCatOwner)['safeWithdraw(address,uint256)'](
        ACCLIMATOR.address,
        ownedMoonCats[0]
      )
    ).to.be.revertedWith('Token is locked', 'Attempt to withdraw locked token')

    // The MoonCat that is locked by just the overriden portal should now be unlocked
    expect(
      await JUMPPORT.isLocked(ACCLIMATOR.address, ownedMoonCats[1])
    ).to.equal(false, 'MoonCat is now unlocked')
    await JUMPPORT.connect(moonCatOwner)['safeWithdraw(address,uint256)'](
      ACCLIMATOR.address,
      ownedMoonCats[1]
    )
    expect(
      await JUMPPORT.isDeposited(ACCLIMATOR.address, ownedMoonCats[1])
    ).to.equal(false, 'MoonCat successfully withdrawn')
    expect(await ACCLIMATOR.ownerOf(ownedMoonCats[1])).to.equal(
      moonCatOwner.address,
      'Transfer back to owner completed'
    )

    // Unlock the token individually from the other portal
    await JUMPPORT.connect(portal1).unlockToken(
      ACCLIMATOR.address,
      ownedMoonCats[0]
    )

    // The MoonCat locked by both portals should now be unlocked
    expect(
      await JUMPPORT.isLocked(ACCLIMATOR.address, ownedMoonCats[0])
    ).to.equal(false, 'MoonCat is now unlocked')
  })
})

async function updateMoonCatOwners(startIndex = 1000, endIndex = 1049) {
  let out = {}
  for (let i = startIndex; i <= endIndex; i++) {
    let owner = await ACCLIMATOR.ownerOf(i)
    if (typeof out[owner] == 'undefined') out[owner] = []
    out[owner].push(i)
  }
  return out
}

async function setup() {
  const accounts = await ethers.getSigners()
  const moonCatOwner = accounts[1]

  // Put all owned MoonCats into JumpPort
  await depositAllOwned(moonCatOwner)

  return {
    moonCatOwner: moonCatOwner,
    EVE: accounts[8],
    ownedMoonCats: moonCatOwners[moonCatOwner.address],
    accounts: accounts,
  }
}

async function depositAllOwned(signer) {
  await ACCLIMATOR.connect(signer).setApprovalForAll(JUMPPORT.address, true)
  await JUMPPORT.connect(signer)['deposit(address,uint256[])'](
    ACCLIMATOR.address,
    moonCatOwners[signer.address]
  )
}

async function testBalance(
  tokenCollection,
  targetAddress,
  expectedBalance,
  expectedBlockHeight = null
) {
  if (Array.isArray(tokenCollection)) {
    expect(
      await JUMPPORT['balanceOf(address[],address)'](
        tokenCollection,
        targetAddress
      )
    ).to.equal(expectedBalance)
  } else {
    let rs = await JUMPPORT['balanceOf(address,address)'](
      tokenCollection,
      targetAddress
    )
    expect(rs).to.have.property('balance')
    expect(rs.balance.toHexString()).to.equal(
      ethers.BigNumber.from(expectedBalance).toHexString()
    )
    if (expectedBlockHeight != null) {
      expect(rs).to.have.property('blockHeight')
      expect(rs.blockHeight.toHexString()).to.equal(
        ethers.BigNumber.from(expectedBlockHeight).toHexString()
      )
    }
  }
}
