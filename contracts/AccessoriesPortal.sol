// SPDX-License-Identifier: AGPL-3.0
pragma solidity ^0.8.9;

import "./Portal.sol";

interface IERC20 {
  function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);
}

/**
 * @dev Portal implementation that allows interacting with the Accessories contract.
 * This allows MoonCats that are deposited into the JumpPort to still be able to purchase
 * and rearrange their accessories.
 */
contract AccessoriesPortal is Portal {
  address public Referrer;
  address public immutable MoonCats;
  address public immutable Accessories;

  mapping(address => uint256) public validNonce; // owner address => minimum valid nonce value
  mapping(uint256 => address) private TokenApprovals; // token ID => operator address
  mapping(address => mapping(address => bool)) private OperatorApprovals; // owner address => operator address => is approved

  event ProxyPurchase(address indexed owner, address indexed operator, uint256 indexed rescueOrder, uint256 accessoryId, uint256 nonce);

  /**
   * @dev Emitted when `owner` enables `approved` to manage the `tokenId` token.
   */
  event Approval(address indexed owner, address indexed approved, uint256 indexed tokenId);

  /**
   * @dev Emitted when `owner` enables or disables (`approved`) `operator` to manage all of its assets.
   */
  event ApprovalForAll(address indexed owner, address indexed operator, bool approved);


  /**
   * @dev Data payload used by the Accessories contract for batch operations.
   */
  struct AccessoryBatchData {
    uint256 rescueOrder;
    uint232 accessory;
    uint8 paletteIndex;
    uint16 zIndex;
  }

  /**
   * @dev Message object that a user can sign to authorise someone else to purchase an Accessory for them.
   */
  struct AccessoryPurchaseRequest {
    uint256 expirationTimestamp;
    uint256 nonce;
    AccessoryBatchData purchaseData;
    address tributeToken;
    uint256 tributeAmount;
  }

  /**
   * @dev The typeHash identifier for an EIP712Domain object, needed for EIP712 messages.
   */
  bytes32 public constant EIP712DOMAIN_TYPEHASH = keccak256(
    "EIP712Domain(string name,string version,uint256 chainId,address verifyingContract)"
  );

  /**
   * @dev The typeHash identifier for an AccessoryBatchData struct, needed for EIP712 messages.
   */
  bytes32 public constant ACCESSORYBATCH_TYPEHASH = keccak256(
    "AccessoryBatchData(uint256 rescueOrder,uint232 accessory,uint8 paletteIndex,uint16 zIndex)"
  );

  /**
   * @dev The typeHash identifier for an AccessoryPurchaseRequest struct, needed for EIP712 messages.
   */
  bytes32 public constant ACCESSORYREQUEST_TYPEHASH = keccak256(
    "AccessoryPurchaseRequest(uint256 expirationTimestamp,uint256 nonce,AccessoryBatchData purchaseData,address tributeToken,uint256 tributeAmount)AccessoryBatchData(uint256 rescueOrder,uint232 accessory,uint8 paletteIndex,uint16 zIndex)"
  );

  /**
   * @dev The domain separator used to identify EIP712 messages used by this specific contract.
   */
  bytes32 public immutable DOMAIN_SEPARATOR;

  constructor (address jumpPortAddress, address referrerAddress, address moonCatAddress, address accessoriesAddress) Portal(jumpPortAddress) {
    Referrer = referrerAddress;
    MoonCats = moonCatAddress;
    Accessories = accessoriesAddress;

    DOMAIN_SEPARATOR = keccak256(abi.encode(
      EIP712DOMAIN_TYPEHASH,
      keccak256(bytes("Accessories Portal")),
      keccak256(bytes("1")),
      block.chainid,
      this
    ));
  }

  /**
   * @dev As an owner of a MoonCat deposited in the JumpPort, purchase an Accessory for them.
   */
  function buyAccessory (uint256 rescueOrder, uint256 accessoryId, uint8 paletteIndex, uint16 zIndex) public payable {
    address owner = JumpPort.ownerOf(MoonCats, rescueOrder);
    require(
      msg.sender == owner || getApproved(rescueOrder) == msg.sender || isApprovedForAll(owner, msg.sender),
      "Buy Accessory caller is not owner nor approved"
    );

    bytes memory payload = abi.encodeWithSignature("buyAccessory(uint256,uint256,uint8,uint16,address)", rescueOrder, accessoryId, paletteIndex, zIndex, Referrer);
    JumpPort.executeAction{ value: msg.value }(owner, MoonCats, rescueOrder, Accessories, payload);
  }

  /**
   * @dev As an owner of a MoonCat deposited in the JumpPort, purchase multiple Accessories for them.
   * The input into this function is different than the `buyAccessories` function on the Accessories contract
   * (this function has inputs of three separate arrays, while the Accessories contract just takes one array of
   * `AccessoryBatchData` records), in order to ensure that all
   */
  function buyAccessories (
    AccessoryBatchData[] calldata orders
  ) public payable{
    require(orders.length >= 2, "Not a multi-order request");
    uint256 rescueOrder = orders[0].rescueOrder;
    address owner = JumpPort.ownerOf(MoonCats, rescueOrder);
    require(
      msg.sender == owner || getApproved(rescueOrder) == msg.sender || isApprovedForAll(owner, msg.sender),
      "Buy Accessories caller is not owner nor approved"
    );
    for(uint256 i = 1; i < orders.length; ++i) {
      require(orders[i].rescueOrder == rescueOrder, "Rescue orders not all the same");
    }
    bytes memory payload = abi.encodeWithSignature("buyAccessories((uint256,uint232,uint8,uint16)[],address)", orders, Referrer);
    JumpPort.executeAction{ value: msg.value }(owner, MoonCats, rescueOrder, Accessories, payload);
  }

  /**
   * @dev As an owner of a MoonCat deposited in the JumpPort, update an Accessory they own.
   */
  function alterAccessory (
    uint256 rescueOrder,
    uint256 ownedAccessoryIndex,
    uint8 paletteIndex,
    uint16 zIndex
  ) public {
    bytes memory payload = abi.encodeWithSignature("alterAccessory(uint256,uint256,uint8,uint16)", rescueOrder, ownedAccessoryIndex, paletteIndex, zIndex);
    JumpPort.executeAction(msg.sender, MoonCats, rescueOrder, Accessories, payload);
  }

  /**
   * @dev Grant a specific address the right to perform actions through this contract as if they were the token's owner.
   * Similar in concept to {IERC721-approve}.
   */
  function approve(address to, uint256 tokenId) public {
    address owner = JumpPort.ownerOf(MoonCats, tokenId);
    require(to != owner, "Approval to current owner");

    require(
      msg.sender == owner || isApprovedForAll(owner, msg.sender),
      "Approve caller is not owner nor approved for all"
    );
    TokenApprovals[tokenId] = to;
    emit Approval(owner, to, tokenId);
  }

  /**
   * @dev Grant a specific address the right to perform actions for all of an other address' tokens in this contract.
   * Similar in concept to {IERC721-setApprovalForAll}.
   */
  function setApprovalForAll(address operator, bool approved ) public {
    require(msg.sender != operator, "Approve to caller");
    OperatorApprovals[msg.sender][operator] = approved;
    emit ApprovalForAll(msg.sender, operator, approved);
  }

  /**
   * @dev For proxy action signatures, set a value to be the first nonce valid for this sender.
   * Invalidates all messages with nonces lower than the submitted value, for that sender.
   */
  function setValidNonce (uint256 firstValidNonce) public {
    require(firstValidNonce > validNonce[msg.sender], "Nonce must increment");
    validNonce[msg.sender] = firstValidNonce;
  }

  /* Proxy Actions */

  /**
   * @dev Get an EIP712-compliant hash of an AccessoryBatchData object.
   */
  function hash(AccessoryBatchData memory x) public pure returns (bytes32) {
    return keccak256(abi.encode(
      ACCESSORYBATCH_TYPEHASH,
      x.rescueOrder,
      x.accessory,
      x.paletteIndex,
      x.zIndex
    ));
  }

  /**
   * @dev Get an EIP712-compliant hash of an AccessoryPurchaseRequest object.
   */
  function hash(AccessoryPurchaseRequest memory x) public pure returns (bytes32) {
    return keccak256(abi.encode(
      ACCESSORYREQUEST_TYPEHASH,
      x.expirationTimestamp,
      x.nonce,
      hash(x.purchaseData),
      x.tributeToken,
      x.tributeAmount
    ));
  }

  /**
   * @dev Get the EIP712 structured-message hash that the owner of a MoonCat needs to sign,
   * to signal they want to allow another to act on their behalf.
   */
  function getProxyMessage (AccessoryPurchaseRequest calldata order) public view returns (bytes32 message) {
    return keccak256(
      abi.encodePacked(
        "\x19\x01",
        DOMAIN_SEPARATOR,
        hash(order)
      )
    );
  }

  /**
   * @dev Verify a signature against an EIP712-structured message
   */
  function validSignature (AccessoryPurchaseRequest calldata order, address expectedSigner, bytes memory signature) public view returns (bool) {
    require(signature.length == 65, "Invalid Signature");

    bytes32 m = getProxyMessage(order);

    uint8 v;
    bytes32 r;
    bytes32 s;
    assembly {
      r := mload(add(signature, 32))
      s := mload(add(signature, 64))
      v := byte(0, mload(add(signature, 96)))
    }

    address signer = ecrecover(m, v, r, s);
    return signer == expectedSigner;
  }

  /**
   * @dev As a non-owner, purchase an Accessory on behalf of the MoonCat's owner.
   * This allows other addresses to "gift" an Accessory to a MoonCat, if their owner allows it.
   * The authorization of the owner is required to ensure that MoonCats don't get "airdropped"
   * spammy/unwanted Accessories. The MoonCat's owner indicates they are in favor of this Accessory
   * purchase by signing an EIP712-structured message to that effect.
   */
  function proxyBuyAccessory (AccessoryPurchaseRequest calldata order, bytes calldata signature) public payable {
    require(block.timestamp < order.expirationTimestamp, "Order has expired");
    address tokenOwner = JumpPort.ownerOf(MoonCats, order.purchaseData.rescueOrder);
    require(order.nonce >= validNonce[tokenOwner], "Order nonce not valid");
    require(validSignature(order, tokenOwner, signature), "Order signature not valid");

    // Order and signature appear valid; proceed with purchase
    bytes memory payload = abi.encodeWithSignature(
      "buyAccessory(uint256,uint256,uint8,uint16,address)",
      order.purchaseData.rescueOrder,
      order.purchaseData.accessory,
      order.purchaseData.paletteIndex,
      order.purchaseData.zIndex,
      Referrer
    );
    JumpPort.executeAction{ value: msg.value }(tokenOwner, MoonCats, order.purchaseData.rescueOrder, Accessories, payload);
    emit ProxyPurchase(tokenOwner, msg.sender, order.purchaseData.rescueOrder, order.purchaseData.accessory, order.nonce);

    // Send tribute, if it was offered
    if (order.tributeAmount > 0) {
      IERC20(order.tributeToken).transferFrom(tokenOwner, msg.sender, order.tributeAmount);
    }
  }

  /**
   * @dev As a non-owner, purchase multiple Accessories on behalf of the MoonCat's owner.
   */
  function proxyBuyAccessories (AccessoryPurchaseRequest[] calldata orders, bytes[] calldata signatures) public payable {
    require(orders.length == signatures.length, "Input length mismatch");
    address tokenOwner = JumpPort.ownerOf(MoonCats, orders[0].purchaseData.rescueOrder);
    uint256 ownerValidNonce = validNonce[tokenOwner];
    AccessoryBatchData[] memory batchData = new AccessoryBatchData[](orders.length);
    for(uint256 i = 0; i < orders.length; ++i) {
      AccessoryPurchaseRequest calldata order = orders[i];
      require(block.timestamp < order.expirationTimestamp, "Order has expired");
      require(order.nonce >= ownerValidNonce, "Order nonce not valid");
      require(validSignature(order, tokenOwner, signatures[i]), "Order signature not valid");

      batchData[i] = order.purchaseData;
      emit ProxyPurchase(tokenOwner, msg.sender, order.purchaseData.rescueOrder, order.purchaseData.accessory, order.nonce);
    }

    bytes memory payload = abi.encodeWithSignature("buyAccessories((uint256,uint232,uint8,uint16)[],address)", batchData, Referrer);
    JumpPort.executeAction{ value: msg.value }(tokenOwner, MoonCats, orders[0].purchaseData.rescueOrder, Accessories, payload);
  }

  /* View Functions */

  /**
   * @dev What address is designated as the current "operator" for a specific token?
   * Similar in concept to {IERC721-getApproved}.
   */
  function getApproved(uint256 tokenId) public view returns (address) {
    require(JumpPort.isDeposited(MoonCats, tokenId), "Approved query for nonexistent token");
    return TokenApprovals[tokenId];
  }

  /**
   * @dev Does a specific address have the rights to act as an "operator" for another address' tokens?
   * Similar in concept to {IERC721-isApprovedForAll}.
   */
  function isApprovedForAll(address owner, address operator) public view  returns (bool) {
    return OperatorApprovals[owner][operator];
  }

}