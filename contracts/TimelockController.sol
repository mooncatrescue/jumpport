// SPDX-License-Identifier: AGPL-3.0
pragma solidity ^0.8.9;

import "./OwnableBase.sol";

error InsufficientSignatures(uint256 minimumRequired, uint256 validAmountSupplied);

/**
 * @dev Group-organizing contract that does governance in an "if there are no objections...?" manner.
 * Based upon:
 * https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/governance/TimelockController.sol
 * https://docs.openzeppelin.com/contracts/4.x/api/governance#timelock
 */
contract TimelockController is OwnableBase {
  bytes32 public constant VOTER_ROLE = keccak256("VOTER_ROLE");
  uint256 internal constant OPERATION_DONE_FLAG = uint256(1);

  mapping(bytes32 => uint256) private scheduledCallBlockHeights;
  uint256 public minimumDelay;
  uint256 public executeEarlyQuorum;

  struct Operation {
    address target;
    bytes payload;
    bytes32 predecessor;
    bytes32 salt;
  }

  struct BatchOperation {
    address[] targets;
    bytes[] payloads;
    bytes32 predecessor;
    bytes32 salt;
  }

  /**
   * @dev The typeHash identifier for an EIP712Domain object, needed for ERIP712 messages.
   */
  bytes32 public constant EIP712DOMAIN_TYPEHASH = keccak256(
    "EIP712Domain(string name,string version,uint256 chainId,address verifyingContract)"
  );

  /**
   * @dev The typeHash identifier for an Operation struct, needed for ERIP712 messages.
   */
  bytes32 public constant OPERATION_TYPEHASH = keccak256(
    "Operation(address target,bytes payload,bytes32 predecessor,bytes32 salt)"
  );

  /**
   * @dev The typeHash identifier for a Batch Operation struct, needed for ERIP712 messages.
   */
  bytes32 public constant BATCHOPERATION_TYPEHASH = keccak256(
    "BatchOperation(address[] targets,bytes[] payloads,bytes32 predecessor,bytes32 salt)"
  );

  /**
   * @dev The domain separator used to identify EIP712 messages used by this specific contract.
   */
  bytes32 public immutable DOMAIN_SEPARATOR;
  
  /**
   * @dev Emitted when a call is scheduled as part of operation `id`.
   */
  event CallScheduled(
    bytes32 indexed id,
    uint256 indexed index,
    address target,
    bytes data,
    bytes32 predecessor,
    uint256 delay
  );

  /**
   * @dev Emitted when a call is performed as part of operation `id`.
   */
  event CallExecuted(bytes32 indexed id, uint256 indexed index, address target, bytes data);

  /**
   * @dev Emitted when operation `id` is cancelled.
   */
  event Cancelled(bytes32 indexed id);

  /**
   * @dev Emitted when the minimum delay for future operations is modified.
   */
  event MinDelayChange(uint256 oldDuration, uint256 newDuration);

  /**
   * @dev Emitted when the minimum quorum for future early executions is modified.
   */
  event ExecuteEarlyQuorumChange(uint256 oldDuration, uint256 newDuration);

  constructor (
    uint256 initialMinimumDelay,
    uint256 initialExecuteEarlyQuorum,
    address[] memory members
  ) {
    // Give DAO members their roles
    unchecked {
      for (uint256 i = 0; i < members.length; i++) {
        roles[VOTER_ROLE][members[i]] = true;
      }
    }

    // Make this contract its own administrator.
    // Thus, the only way to call `onlyRole(ADMIN_ROLE)` functions is via a scheduled call
    roles[ADMIN_ROLE][address(this)] = true;

    // Revoke the default granting of the Administrator role to the deployer that the OwnableBase constructor does
    roles[ADMIN_ROLE][msg.sender] = false;

    minimumDelay = initialMinimumDelay;
    emit MinDelayChange(0, minimumDelay);

    executeEarlyQuorum = initialExecuteEarlyQuorum;
    emit ExecuteEarlyQuorumChange(0, executeEarlyQuorum);

    DOMAIN_SEPARATOR = keccak256(abi.encode(
      EIP712DOMAIN_TYPEHASH,
      keccak256(bytes("Timelock Governance")),
      keccak256(bytes("1")),
      block.chainid,
      this
    ));
  }

  /**
   * @dev Returns whether an id correspond to a registered operation. This
   * includes both Pending, Ready and Done operations.
   */
  function isOperation (bytes32 id) public view returns (bool registered) {
    return scheduledCallBlockHeights[id] > 0;
  }

  /**
   * @dev Returns whether an operation is pending or not.
   */
  function isOperationPending (bytes32 id) public view returns (bool pending) {
    return scheduledCallBlockHeights[id] > OPERATION_DONE_FLAG;
  }

  /**
   * @dev Returns whether an operation is ready or not.
   */
  function isOperationReady (bytes32 id) public view returns (bool ready) {
    uint256 blockHeight = scheduledCallBlockHeights[id];
    return blockHeight > OPERATION_DONE_FLAG && blockHeight <= block.number;
  }

  /**
   * @dev Returns whether an operation is done or not.
   */
  function isOperationDone (bytes32 id) public view returns (bool done) {
    return scheduledCallBlockHeights[id] == OPERATION_DONE_FLAG;
  }

  /**
   * @dev Returns the block height at with an operation becomes ready (0 for
   * unset operations, `OPERATION_DONE_FLAG` for done operations).
   */
  function getBlockHeight (bytes32 id) public view returns (uint256 blockHeight) {
    return scheduledCallBlockHeights[id];
  }

  /**
   * @dev Get an EIP712-compliant hash of an Operation object.
   */
  function hash (Operation memory x) public pure returns (bytes32) {
    return keccak256(abi.encode(
      OPERATION_TYPEHASH,
      x.target,
      keccak256(x.payload),
      x.predecessor,
      x.salt
    ));
  }

  /**
   * @dev Get an EIP712-compliant hash of a BatchOperation object.
   */
  function hash (BatchOperation memory x) public pure returns (bytes32) {
    bytes32[] memory payloadHashes = new bytes32[](x.payloads.length);
    for (uint256 i = 0; i < x.payloads.length; ++i) {
      payloadHashes[i] = keccak256(x.payloads[i]);
    }

    return keccak256(abi.encode(
      BATCHOPERATION_TYPEHASH,
      keccak256(abi.encodePacked(x.targets)),
      keccak256(abi.encodePacked(payloadHashes)),
      x.predecessor,
      x.salt
    ));
  }

  /**
   * @dev Get the EIP712 structured-message hash that a voter needs to sign,
   * to signal they agree with an operation being executed.
   */
  function getProxyMessage (Operation memory operation) public view returns (bytes32 message) {
    return keccak256(
      abi.encodePacked(
        "\x19\x01",
        DOMAIN_SEPARATOR,
        hash(operation)
      )
    );
  }

  /**
   * @dev Get the EIP712 structured-message hash that a voter needs to sign,
   * to signal they agree with a batch operation being executed.
   */
  function getProxyMessage (BatchOperation memory operation) public view returns (bytes32 message) {
    return keccak256(
      abi.encodePacked(
        "\x19\x01",
        DOMAIN_SEPARATOR,
        hash(operation)
      )
    );
  }

  /**
   * @dev Get the EIP712 structured-message hash that a voter needs to sign,
   * to signal they agree with an operation being executed.
   */
  function getProxyMessage (bytes32 objectHash) public view returns (bytes32 message) {
    return keccak256(
      abi.encodePacked(
        "\x19\x01",
        DOMAIN_SEPARATOR,
        objectHash
      )
    );
  }

  /**
   * @dev Verify a signature against an EIP712-structured message
   */
  function validSignature (bytes32 message, bytes memory signature) public view returns (bool) {
    require(signature.length == 65, "Invalid Signature");

    uint8 v;
    bytes32 r;
    bytes32 s;
    assembly {
      r := mload(add(signature, 32))
      s := mload(add(signature, 64))
      v := byte(0, mload(add(signature, 96)))
    }

    address signer = ecrecover(message, v, r, s);
    return roles[VOTER_ROLE][signer] == true;
  }

  /**
   * @dev Schedule an operation containing a single transaction.
   */
  function schedule (
    address target,
    bytes calldata data,
    bytes32 predecessor,
    bytes32 salt,
    uint256 delay
  ) public onlyRole(VOTER_ROLE) {
    bytes32 id = hash(Operation(target, data, predecessor, salt));
    _schedule(id, delay);
    emit CallScheduled(id, 0, target, data, predecessor, delay);
  }

  /**
   * @dev Schedule an operation containing a batch of transactions.
   */
  function scheduleBatch (
    address[] calldata targets,
    bytes[] calldata payloads,
    bytes32 predecessor,
    bytes32 salt,
    uint256 delay
  ) public onlyRole(VOTER_ROLE) {
    require(targets.length == payloads.length, "TimelockController: length mismatch");

    bytes32 id = hash(BatchOperation(targets, payloads, predecessor, salt));
    _schedule(id, delay);
    for (uint256 i = 0; i < targets.length; ++i) {
      emit CallScheduled(id, i, targets[i], payloads[i], predecessor, delay);
    }
  }

  /**
   * @dev Schedule an operation that is to becomes valid after a given delay.
   */
  function _schedule (bytes32 id, uint256 delay) private {
    require(!isOperation(id), "TimelockController: operation already scheduled");
    require(delay >= minimumDelay, "TimelockController: insufficient delay");
    scheduledCallBlockHeights[id] = block.number + delay;
  }

  /**
   * @dev Cancel an operation.
   */
  function cancel (bytes32 id) public onlyRole(VOTER_ROLE) {
    require(isOperationPending(id), "TimelockController: operation cannot be cancelled");
    delete scheduledCallBlockHeights[id];

    emit Cancelled(id);
  }

  /**
   * @dev Cancel multiple operations.
   */
  function cancelMultiple (bytes32[] calldata ids) public onlyRole(VOTER_ROLE) {
    unchecked {
      for(uint256 i = 0; i < ids.length; ++i) {
        require(isOperationPending(ids[i]), "TimelockController: operation cannot be cancelled");
        delete scheduledCallBlockHeights[ids[i]];

        emit Cancelled(ids[i]);
      }
    }
  }

  /**
   * @dev Execute an (ready) operation containing a single transaction.
   * This function can reenter, but it doesn't pose a risk because _afterCall checks that the proposal is pending,
   * thus any modifications to the operation during reentrancy should be caught.
   */
  function execute (
    address target,
    bytes calldata payload,
    bytes32 predecessor,
    bytes32 salt
  ) public {
    bytes32 id = hash(Operation(target, payload, predecessor, salt));

    _beforeCall(id, predecessor);
    _doCall(id, 0, target, payload);
    _afterCall(id);
  }

  /**
   * @dev Execute an (ready) operation containing a batch of transactions.
   */
  function executeBatch (
    address[] calldata targets,
    bytes[] calldata payloads,
    bytes32 predecessor,
    bytes32 salt
  ) public {
    require(targets.length == payloads.length, "TimelockController: length mismatch");

    bytes32 id = hash(BatchOperation(targets, payloads, predecessor, salt));

    _beforeCall(id, predecessor);
    for (uint256 i = 0; i < targets.length; ++i) {
      _doCall(id, i, targets[i], payloads[i]);

    }
    _afterCall(id);
  }

  /**
   * @dev Execute an (ready) operation containing a single transaction early, using signatures of other voters.
   */
  function executeEarly (
    address target,
    bytes calldata payload,
    bytes32 predecessor,
    bytes32 salt,
    bytes[] calldata signatures
  ) public {
    bytes32 id = hash(Operation(target, payload, predecessor, salt));
    _beforeEarlyCall(id, predecessor, signatures);
    _doCall(id, 0, target, payload);
    _afterEarlyCall(id);
  }

  /**
   * @dev Execute an (ready) operation containing a batch of transactions early, using signatures of other voters.
   */
  function executeBatchEarly (
    address[] calldata targets,
    bytes[] calldata payloads,
    bytes32 predecessor,
    bytes32 salt,
    bytes[] calldata signatures
  ) public {
    require(targets.length == payloads.length, "TimelockController: length mismatch");

    bytes32 id = hash(BatchOperation(targets, payloads, predecessor, salt));
    _beforeEarlyCall(id, predecessor, signatures);

    for (uint256 i = 0; i < targets.length; ++i) {
      _doCall(id, i, targets[i], payloads[i]);
    }

    _afterEarlyCall(id);
  }

  /**
   * @dev Schedule an operation containing a single transaction,
   * and immediately execute it, using signatures of other voters.
   */
  function scheduleAndExecute (
    address target,
    bytes calldata data,
    bytes32 predecessor,
    bytes32 salt,
    bytes[] calldata signatures
  ) public {
    schedule(target, data, predecessor, salt, minimumDelay);
    executeEarly(target, data, predecessor, salt, signatures);
  }

  /**
   * @dev Schedule an operation containing a batch of transactions,
   * and immediately execute it, using signatures of other voters.
   */
  function scheduleAndExecuteBatch (
    address[] calldata targets,
    bytes[] calldata payloads,
    bytes32 predecessor,
    bytes32 salt,
    bytes[] calldata signatures
  ) public {
    scheduleBatch(targets, payloads, predecessor, salt, minimumDelay);
    executeBatchEarly(targets, payloads, predecessor, salt, signatures);
  }

  /**
   * @dev Checks before execution of an operation's calls.
   */
  function _beforeCall (bytes32 id, bytes32 predecessor) private view {
    require(isOperationReady(id), "TimelockController: operation is not ready");
    require(predecessor == bytes32(0) || isOperationDone(predecessor), "TimelockController: missing dependency");
  }

  /**
   * @dev Make a call on an external contract, catching any error it returns.
   */
  function _doCall (bytes32 id, uint256 index, address target, bytes memory payload) private {
    (bool success, bytes memory returnData) = target.call(payload);
    if (success == false) {
      if (returnData.length == 0) {
        revert("TimelockController: underlying transaction reverted");
      } else {
        assembly {
          revert(add(32, returnData), mload(returnData))
        }
      }
    } else {
      emit CallExecuted(id, index, target, payload);
    }
  }

  /**
   * @dev Checks after execution of an operation's calls.
   */
  function _afterCall (bytes32 id) private {
    require(isOperationReady(id), "TimelockController: operation is not ready");
    scheduledCallBlockHeights[id] = OPERATION_DONE_FLAG;
  }

  /**
   * @dev Checks before early execution of an operation's calls.
   */
  function _beforeEarlyCall (bytes32 id, bytes32 predecessor, bytes[] calldata signatures) private view {
    // Use "pending", not "ready" to verify the call does exist and has not been run, but might be before the timelock expires
    require(isOperationPending(id), "TimelockController: operation is not pending");
    require(predecessor == bytes32(0) || isOperationDone(predecessor), "TimelockController: missing dependency");

    // Check signatures
    bytes32 message = getProxyMessage(id);
    uint256 numValid = 0;
    for (uint256 i = 0; i < signatures.length; ++i) {
      if (validSignature(message, signatures[i])) {
        numValid++;
      }
    }
    if (roles[VOTER_ROLE][msg.sender] == true) {
      numValid++;
    }
    if (numValid < executeEarlyQuorum) revert InsufficientSignatures(executeEarlyQuorum, numValid);
  }

  /**
   * @dev Checks after early execution of an operation's calls.
   */
  function _afterEarlyCall (bytes32 id) private {
    // Use "pending", not "ready" to verify the call does exist and has not been run, but might be before the timelock expires
    require(isOperationPending(id), "TimelockController: operation is not pending");
    scheduledCallBlockHeights[id] = OPERATION_DONE_FLAG;
  }

  /**
   * @dev Changes the minimum timelock duration for future operations.
   */
  function updateDelay (uint256 newDelay) public onlyRole(ADMIN_ROLE) {
    emit MinDelayChange(minimumDelay, newDelay);
    minimumDelay = newDelay;
  }

  /**
   * @dev Changes the minimum quorum level for future early executions.
   */
  function updateQuorum (uint256 newQuorum) public onlyRole(ADMIN_ROLE) {
    emit ExecuteEarlyQuorumChange(executeEarlyQuorum, newQuorum);
    executeEarlyQuorum = newQuorum;
  }

  /**
   * @dev Allow current administrators to be able to grant/revoke voter role to other addresses.
   */
  function setVoter (address account, bool isVoter) public onlyRole(ADMIN_ROLE) {
    roles[VOTER_ROLE][account] = isVoter;
    emit RoleChange(VOTER_ROLE, account, isVoter, msg.sender);
  }

}
