// SPDX-License-Identifier: AGPL-3.0
pragma solidity ^0.8.9;

import "./Portal.sol";
import { IERC20 } from "./OwnableBase.sol";

/**
 * @dev Portal implementation that rewards long-term locking with ERC20 rewards.
 */
contract ERC20StakingPortal is Portal {
  address public RewardToken;
  address public StakedToken;
  bool public stakingActive = false;
  uint256 internal globalStartBlock;

  struct RewardTier {
    uint128 minBlocksStaked;
    uint128 rewardAmount;
  }
  RewardTier[] public RewardTiers;

  mapping(uint256 => uint256) internal tokenStakeStartBlock;
  mapping(uint256 => uint256) internal tokenLastClaimBlock;

  constructor (address jumpPortAddress) Portal(jumpPortAddress) {
    globalStartBlock = block.number;
  }

  function getRewardAmount (uint256 blocksStakedDuration) public view returns (uint128 rewardAmount) {
    unchecked {
      if (RewardTiers.length == 0) return 0;
      for (uint256 i = RewardTiers.length; i > 0; i--) {
        if (RewardTiers[i-1].minBlocksStaked <= blocksStakedDuration) {
          return RewardTiers[i-1].rewardAmount;
        }
      }
      return 0;
    }
  }

  function getStakedDuration (uint256 tokenId) public view returns (uint256 startBlock, uint256 duration) {
    require(tokenStakeStartBlock[tokenId] > 0, "Not currently staked");
    startBlock = tokenStakeStartBlock[tokenId];
    duration = block.number - startBlock;
  }

  function stakeToken (uint256 tokenId) public {
    require(RewardTiers.length > 0 && RewardToken != address(0) && StakedToken != address(0), "Staking rewards not configured");
    require(stakingActive == true, "Staking not active");
    require(tokenStakeStartBlock[tokenId] == 0, "Already staked");

    require(JumpPort.isDeposited(StakedToken, tokenId) == true, "Token not in JumpPort");
    address tokenOwner = JumpPort.ownerOf(StakedToken, tokenId);
    require(tokenOwner == msg.sender || JumpPort.getApproved(StakedToken, tokenId) == msg.sender || JumpPort.isApprovedForAll(tokenOwner, msg.sender) == true, "Not an operator of that token");

    // Determine a start block for this token
    uint256 stakeStartBlock = tokenLastClaimBlock[tokenId];
    if (stakeStartBlock == 0) {
      // Token has never claimed; start session from when they first entered the JumpPort
      stakeStartBlock = JumpPort.depositedSince(StakedToken, tokenId);
      if (stakeStartBlock < globalStartBlock) {
        // Don't allow retroactive claiming before the global start of this reward contract
        stakeStartBlock = globalStartBlock;
      }
    } else {
      unchecked { stakeStartBlock++; }
    }

    // Start staking timer for this token
    tokenStakeStartBlock[tokenId] = stakeStartBlock;
    JumpPort.lockToken(StakedToken, tokenId);
  }

  function claimReward (uint256 tokenId) public {
    require(RewardTiers.length > 0 && RewardToken != address(0) && StakedToken != address(0), "Staking rewards not configured");
    require(tokenStakeStartBlock[tokenId] > 0, "Not currently staked");

    require(JumpPort.isDeposited(StakedToken, tokenId) == true, "Token not in JumpPort");
    address tokenOwner = JumpPort.ownerOf(StakedToken, tokenId);
    require(tokenOwner == msg.sender || JumpPort.getApproved(StakedToken, tokenId) == msg.sender || JumpPort.isApprovedForAll(tokenOwner, msg.sender) == true, "Not an operator of that token");

    uint128 rewardAmount = getRewardAmount(block.number - tokenStakeStartBlock[tokenId]);

    // Mark this token as no longer staked
    tokenLastClaimBlock[tokenId] = block.number;
    tokenStakeStartBlock[tokenId] = 0;
    JumpPort.unlockToken(StakedToken, tokenId);

    // Pay reward
    if (rewardAmount > 0) {
      IERC20(RewardToken).transfer(tokenOwner, rewardAmount);
    }
  }

  /* Administration */

  function setTargetTokens (address rewardTokenAddress, address stakedTokenAddress) public onlyRole(ADMIN_ROLE) {
    RewardToken = rewardTokenAddress;
    StakedToken = stakedTokenAddress;
  }

  function setGlobalStart (uint256 startBlock) public onlyRole(ADMIN_ROLE) {
    globalStartBlock = startBlock;
  }

  function setEnabled (bool isEnabled) public onlyRole(ADMIN_ROLE) {
    stakingActive = isEnabled;
  }

  function setRewardTiers (uint128[] calldata minBlocksAmounts, uint128[] calldata rewardAmounts) public onlyRole(ADMIN_ROLE) {
    require(minBlocksAmounts.length == rewardAmounts.length, "Inputs mismatched");
    delete RewardTiers;
    unchecked {
      for (uint256 i = 0; i < minBlocksAmounts.length; i++) {
        if (i > 0) {
          require(minBlocksAmounts[i] > RewardTiers[i - 1].minBlocksStaked, "Tiers not in ascending order");
        }
        RewardTiers.push(RewardTier(minBlocksAmounts[i], rewardAmounts[i]));
      }
    }
  }
}