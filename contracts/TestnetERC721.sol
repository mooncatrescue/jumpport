// SPDX-License-Identifier: AGPL-3.0
pragma solidity ^0.8.9;

import { ERC721Enumerable, ERC721 } from "@openzeppelin/contracts/token/ERC721/extensions/ERC721Enumerable.sol";
import { IMoonCatAcclimator, IERC721, IERC721Enumerable, IERC721Metadata, IERC165 } from "@mooncatrescue/contracts/IMoonCatAcclimator.sol";
import { OwnableBase } from "./OwnableBase.sol";

error NotImplemented();

/**
 * Link the required function definitions in {IMoonCatAcclimator} to the implementation.
 *
 * Because the generic {ERC721Enumerable} contract doesn't know of {IMoonCatAcclimator}, it isn't marked as inheriting
 * that interface, and so the Solidity compiler requires functions that share the same name be explicitly defined as overriding
 * both branches of inheritence, to mark them as not just named similarly, but conceptually/functionally the same thing.
 */
abstract contract ERC721Merged is IMoonCatAcclimator, ERC721Enumerable {

  function approve (address to, uint256 tokenId) public override(ERC721, IERC721) {
    ERC721.approve(to, tokenId);
  }

  function balanceOf (address owner) public view override(ERC721, IERC721) returns (uint256) {
    return ERC721.balanceOf(owner);
  }

  function getApproved (uint256 tokenId) public view override(ERC721, IERC721) returns (address) {
    return ERC721.getApproved(tokenId);
  }

  function isApprovedForAll (address owner, address operator) public view override(ERC721, IERC721) returns (bool) {
    return ERC721.isApprovedForAll(owner, operator);
  }

  function name () public view override(ERC721, IERC721Metadata) returns (string memory) {
    return ERC721.name();
  }

  function ownerOf (uint256 tokenId) public view override(ERC721, IERC721) returns (address) {
    return ERC721.ownerOf(tokenId);
  }

  function safeTransferFrom (
    address from,
    address to,
    uint256 tokenId
  ) public override(ERC721, IERC721) {
    return ERC721.safeTransferFrom(from, to, tokenId, ""); // Send directly to the "safe transfer from with data" function
  }

  function safeTransferFrom (
    address from,
    address to,
    uint256 tokenId,
    bytes calldata data
  ) public override(ERC721, IERC721) {
    return ERC721.safeTransferFrom(from, to, tokenId, data);
  }

  function setApprovalForAll (address operator, bool approved) public override(ERC721, IERC721) {
    return ERC721.setApprovalForAll(operator, approved);
  }

  function supportsInterface (bytes4 interfaceId) public view override(ERC721Enumerable, IERC165) returns (bool) {
    return ERC721Enumerable.supportsInterface(interfaceId);
  }

  function symbol () public view override(ERC721, IERC721Metadata) returns (string memory) {
    return ERC721.symbol();
  }

  function tokenByIndex (uint256 index) public view override(ERC721Enumerable, IERC721Enumerable) returns (uint256) {
    return ERC721Enumerable.tokenByIndex(index);
  }

  function tokenOfOwnerByIndex (address owner, uint256 index) public view override(ERC721Enumerable, IERC721Enumerable) returns (uint256) {
    return ERC721Enumerable.tokenOfOwnerByIndex(owner, index);
  }

  function tokenURI (uint256 tokenId) public view override(ERC721, IERC721Metadata) returns (string memory) {
    return ERC721.tokenURI(tokenId);
  }

  function totalSupply () public view override(ERC721Enumerable, IERC721Enumerable) returns (uint256) {
    return ERC721Enumerable.totalSupply();
  }

  function transferFrom (
    address from,
    address to,
    uint256 tokenId
  ) public override(ERC721, IERC721) {
    ERC721.transferFrom(from, to, tokenId);
  }
}


contract TestnetERC721 is ERC721Merged, OwnableBase {
  string baseURI;

  constructor () ERC721("Chimera", "CHI") { }

  /**
   * @dev Override {ERC721-_baseURI}.
   */
  function _baseURI () internal view override returns (string memory) {
    return baseURI;
  }

  /**
   * @dev Allow administrator to make any token they want.
   */
  function mint (address to, uint256 tokenId) public onlyRole(ADMIN_ROLE) {
    require(tokenId < 25440, "Unable to mint that Chimera");
    _mint(to, tokenId);
  }

  /**
   * @dev Allow administrator to claim ownership of any token they want.
   */
  function clawback (address to, uint256 tokenId) public onlyRole(ADMIN_ROLE) {
    require(_exists(tokenId), "Clawback request on nonexistent token");
    address owner = ERC721.ownerOf(tokenId);
    _transfer(owner, to, tokenId);
  }

  /**
   * @dev Set the base URL location for token metadata query results.
   */
  function setBaseURI (string memory newBaseURI) public onlyRole(ADMIN_ROLE) {
    baseURI = newBaseURI;
  }


  /**
   * @dev Replicate custom owner enumeration from MoonCat Acclimator contract.
   */
  function tokensIdsByOwner (address owner) public view override returns (uint256[] memory) {
    uint256 totalOwned = balanceOf(owner);
    uint256[] memory tokens = new uint256[](totalOwned);
    unchecked {
      for (uint i = 0; i < totalOwned; ++i) {
        tokens[i] = tokenOfOwnerByIndex(owner, i);
      }
    }
    return tokens;
  }

  ///// Implement aliases for "mint" /////

  function wrap (uint256 tokenId) public override onlyRole(ADMIN_ROLE) {
    mint(msg.sender, tokenId);
  }

  function buyAndWrap (uint256 tokenId) public payable override onlyRole(ADMIN_ROLE) {
    mint(msg.sender, tokenId);
  }

  function batchWrap (uint256[] memory tokenIds) public override onlyRole(ADMIN_ROLE) {
    for (uint256 i = 0; i < tokenIds.length; ++i) {
      mint(msg.sender, tokenIds[i]);
    }
  }

  function batchReWrap (uint256[] memory tokenIds, uint256[] memory ) public override {
    batchWrap(tokenIds);
  }

  ///// Stub in remaining functions /////

  function getChild (address, uint256, address, uint256) public pure override { revert NotImplemented(); }

  function ownerOfChild (address, uint256)
    public
    pure
    override
    returns (bytes32, uint256) { revert NotImplemented(); }

  function rootOwnerOf (uint256) public pure override returns (bytes32) { revert NotImplemented(); }

  function rootOwnerOfChild (address, uint256)
    public
    pure
    override
    returns (bytes32) { revert NotImplemented(); }

  function safeTransferChild (uint256, address, address, uint256) public pure override { revert NotImplemented(); }

  function safeTransferChild (uint256, address, address, uint256, bytes calldata) public pure override { revert NotImplemented(); }

  function transferChild (uint256, address, address, uint256) public pure override { revert NotImplemented(); }

  function transferChildToParent (uint256, address, uint256, address, uint256, bytes calldata) public pure override { revert NotImplemented(); }

  function totalChildContracts (uint256) public pure override returns (uint256) { revert NotImplemented(); }

  function childContractByIndex (uint256, uint256)
    public
    pure
    override
    returns (address) { revert NotImplemented(); }

  function totalChildTokens (uint256, address)
    public
    pure
    override
    returns (uint256) { revert NotImplemented(); }

  function childTokenByIndex (uint256, address, uint256) public pure override returns (uint256) { revert NotImplemented(); }

  function onERC721Received (address, address, uint256, bytes calldata) public pure override returns (bytes4) { revert NotImplemented(); }

  function pause () public pure override { revert NotImplemented(); }

  function unpause () public pure override { revert NotImplemented(); }

  function unwrap (uint256) public pure override { revert NotImplemented(); }

  function batchUnwrap (uint256[] memory) public pure override { revert NotImplemented(); }

}
