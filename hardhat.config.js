/**
 * @type import('hardhat/config').HardhatUserConfig
 */
require('dotenv').config();
require("@nomiclabs/hardhat-ethers");
require("@nomiclabs/hardhat-etherscan");
require("@nomiclabs/hardhat-waffle");

const { API_URL, FORK_BLOCK, ETHERSCAN_API_KEY, ETHERSCAN_OPTIMISM_API_KEY,
  ETHERSCAN_ARBITRUM_API_KEY, ETHERSCAN_GNOSIS_API_KEY, ETHERSCAN_MATIC_API_KEY } = process.env
if (typeof API_URL == 'undefined') {
  // Make this error a bit more evident. If we don't manually check,
  // the message Hardhat gives isn't that helpful to determine the root cause
  console.error('No API_URL defined!');
  process.exit(1);
}

module.exports = {
  defaultNetwork: "hardhat",
  networks: {
    hardhat: {
      allowUnlimitedContractSize: true,
      chainId: 1337,
      forking: {
        url: API_URL,
        blockNumber: parseInt(FORK_BLOCK)
      }
    },
    docker: {
      url: "http://hardhat-node:8545"
    },
    goerli: {
      url: 'https://goerli.infura.io/v3/84842078b09946638c03157f83405213'
    },
    optimisticEthereum: {
      url: 'https://mainnet.optimism.io'
    },
    arbitrumOne: {
      url: 'https://arb1.arbitrum.io/rpc'
    },
    xdai: {
      url: 'https://rpc.gnosischain.com'
    },
    gnosis: {
      url: 'https://rpc.gnosischain.com'
    },
    polygon: {
      url: 'https://polygon-rpc.com'
    },
  },
  etherscan: {
    apiKey: {
      mainnet: ETHERSCAN_API_KEY,
      goerli: ETHERSCAN_API_KEY,
      optimisticEthereum: ETHERSCAN_OPTIMISM_API_KEY,
      arbitrumOne: ETHERSCAN_ARBITRUM_API_KEY,
      xdai: ETHERSCAN_GNOSIS_API_KEY,
      gnosis: ETHERSCAN_GNOSIS_API_KEY,
      polygon: ETHERSCAN_MATIC_API_KEY
    },
    customChains: [
      {
        network: "gnosis",
        chainId: 100,
        urls: {
          apiURL: "https://api.gnosisscan.io/api",
          browserURL: "https://gnosisscan.io",
        }
      }
    ]
  },
  solidity: {
    version: "0.8.9",
    settings: {
      optimizer: {
        enabled: true,
        runs: 200
      }
    }
  },

  paths: {
    sources: "./contracts",
    tests: "./test",
    cache: "./cache",
    artifacts: "./artifacts"
  },
  mocha: {
    timeout: 60000,
    slow: 50000
  }
}
