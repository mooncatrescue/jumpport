require('dotenv').config()
const { network, ethers } = require('hardhat'),
  {
    mineUntilGasTarget,
    JumpPort,
    MoonCatAcclimator,
    MoonCatAccessories,
  } = require('@mooncatrescue/contracts/moonCatUtils')

async function deployContracts(verbose = false) {
  const accounts = await ethers.getSigners()
  await mineUntilGasTarget(ethers.utils.parseUnits('5', 'gwei'))

  const MOONCAT_REFERENCE_ADDRESS = '0x0B78C64bCE6d6d4447e58b09E53F3621f44A2a48'
  const ACCLIMATOR = await MoonCatAcclimator
  const ACC = await MoonCatAccessories
  const JUMPPORT = await JumpPort
  if (verbose) {
    console.log('Connecting to JumpPort at ' + JUMPPORT.address)
  }

  // Set first Hardhat account as an admin of the JumpPort
  const jumpportAdmin = '0xdf2E60Af57C411F848B1eA12B10a404d194bce27'
  await accounts[5].sendTransaction({
    to: jumpportAdmin,
    value: ethers.utils.parseEther('0.3'),
  })
  await network.provider.request({
    method: 'hardhat_impersonateAccount',
    params: [jumpportAdmin],
  })
  let adminSigner = await ethers.provider.getSigner(jumpportAdmin)
  await JUMPPORT.connect(adminSigner).setAdmin(accounts[0].address, true)

  const AccessoriesPortal = await ethers.getContractFactory('AccessoriesPortal')
  const ACCESSORIES = await AccessoriesPortal.deploy(
    MOONCAT_REFERENCE_ADDRESS,
    JUMPPORT.address,
    accounts[0].address,
    ACCLIMATOR.address,
    ACC.address
  )
  if (verbose)
    console.log('Accessories Portal Contract Deployed To:', ACCESSORIES.address)
  await JUMPPORT.setPortalValidation(ACCESSORIES.address, true)

  return {
    JUMPPORT,
    ACCESSORIES,
  }
}

module.exports = {
  deployContracts,
}
