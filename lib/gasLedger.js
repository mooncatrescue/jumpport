const { ethers } = require('hardhat'),
  path = require('path'),
  fs = require('fs');

let gasLedger = {};
let defaultGas = ethers.utils.parseUnits('120', 'gwei')

async function gasUsed(tx, grouping, txDesc, gasCost = defaultGas) {
  let txReceipt = await ethers.provider.getTransactionReceipt(tx.hash);
  let gasUsed;
  if (typeof txReceipt.cumulativeGasUsed != 'undefined') {
    gasUsed = txReceipt.cumulativeGasUsed.toNumber();
  } else {
    gasUsed = txReceipt.gasUsed.toNumber();
  }
  if (typeof gasLedger[grouping] == 'undefined') {
    gasLedger[grouping] = [];
  }
  gasLedger[grouping].push({ desc: txDesc, gas: gasUsed, gasCost: gasCost });
  return gasUsed;
}

function gasEstimate(gasUsed, grouping, txDesc, gasCost = defaultGas) {
  if (typeof gasLedger[grouping] == 'undefined') {
    gasLedger[grouping] = [];
  }
  gasLedger[grouping].push({ desc: txDesc, gas: gasUsed, gasCost: gasCost });
}

function gasToEthCost(gasAmount, gasCost) {
  return ethers.utils.formatEther(gasCost.mul(gasAmount));
}

function reportGasUsage() {
  for(grouping of Object.keys(gasLedger)) {
    console.log(grouping + ':');
    gasLedger[grouping].forEach(({ desc, gas, gasCost }) => {
      console.log(`  ${desc}: ${gas.toLocaleString()} gas (${gasToEthCost(gas, gasCost)} ETH @ ${ethers.utils.formatUnits(gasCost, 'gwei')} gwei gas cost)`);
    });
    console.log('');
  }
}

function exportLedger(filename) {
  let formatted = {};
  let hasData = false;
  for(grouping of Object.keys(gasLedger)) {
    formatted[grouping] = [];
    formatted[grouping] = gasLedger[grouping].map(record => {
      record.gasCost = `${gasToEthCost(record.gas, record.gasCost)} ETH @ ${ethers.utils.formatUnits(record.gasCost, 'gwei')} gwei gas cost`;
      hasData = true;
      return record;
    });
  }
  let data = JSON.stringify(formatted, null, 2);
  if (hasData) {
    let outputFile = path.basename(filename, path.extname(filename)) + '.gas.json';
    fs.writeFileSync(outputFile, data);
  }

  gasLedger = []; // Reset storage
  return data;
}

module.exports = {
  gasUsed,
  gasEstimate,
  reportGasUsage,
  exportLedger
}