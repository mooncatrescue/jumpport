const { network, ethers } = require('hardhat'),
  { disperseMoonCats, disperseLootprints } = require('@mooncatrescue/contracts/moonCatUtils');

async function main() {
  await disperseMoonCats(1000, 1004, 5); // Give one MoonCat to the first five addresses
  await disperseMoonCats(5050, 5059, 2); // Give five more MoonCats to the first two addresses
  await disperseLootprints(21000, 21050, 4); // Give lootprints to the first four addresses
}

main()
.then(() => process.exit(0))
.catch(error => {
  console.error(error);
  process.exit(1);
});
