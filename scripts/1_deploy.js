const { network, ethers } = require('hardhat'),
  { mineUntilGasTarget } = require('@mooncatrescue/contracts/moonCatUtils'),
  { deployContracts } = require('../lib/deploy')

async function main() {
  ///// Setup needed contracts /////
  await deployContracts(true)

  ///// Settle gas prices /////
  await mineUntilGasTarget(ethers.utils.parseUnits('5', 'gwei'))
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
