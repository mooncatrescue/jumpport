const { network, ethers } = require("hardhat"),
  { MoonCatAcclimator, JumpPort } = require('@mooncatrescue/contracts/moonCatUtils'),
  { MOONCAT_REFERENCE_ADDRESS } = process.env;

async function main() {
  // Assumes that 1_deploy and 2_distribute-assets has already been run

  const accounts = await ethers.getSigners();
  const JUMPPORT = await JumpPort

  MOONCAT_REFERENCE_ADDRESS = '0x0B78C64bCE6d6d4447e58b09E53F3621f44A2a48'

  const StakingPortal = await ethers.getContractFactory("ERC20StakingPortal");
  const PORTAL = await StakingPortal.deploy(MOONCAT_REFERENCE_ADDRESS, JUMPPORT.address);
  console.log("Staking Portal Contract Deployed To:", PORTAL.address);

  // Validate Portal on JumpPort
  await JUMPPORT.setPortalValidation(PORTAL.address, true);

  // Move MoonCat 1000, 5050 and 5054 to JumpPort (Owned by Account 0)
  let ACCLIMATOR = await MoonCatAcclimator;
  await ACCLIMATOR["safeTransferFrom(address,address,uint256)"](accounts[0].address, JUMPPORT.address, 1000);
  console.log(`Transfer MoonCat #1000 to JumpPort`);
  await ACCLIMATOR["safeTransferFrom(address,address,uint256)"](accounts[0].address, JUMPPORT.address, 5050);
  console.log(`Transfer MoonCat #5050 to JumpPort`);
  await ACCLIMATOR["safeTransferFrom(address,address,uint256)"](accounts[0].address, JUMPPORT.address, 5054);
  console.log(`Transfer MoonCat #5054 to JumpPort`);

  // Setup Staking
  const DAI = new ethers.Contract("0x6b175474e89094c44da98b954eedeac495271d0f", [
    "function balanceOf(address account) external view returns (uint256)",
    "function transfer(address recipient, uint256 amount) external returns (bool)",
  ]);
  await PORTAL.setEnabled(true);
  await PORTAL.setTargetTokens("0x6b175474e89094c44da98b954eedeac495271d0f", ACCLIMATOR.address);
  await PORTAL.setGlobalStart(12345);
  await PORTAL.setRewardTiers([5, 10, 20], [100, 500, 10000]);

  // Stake (and therefore Lock) MoonCat 1000 and 5054
  await PORTAL.stakeToken(1000);
  console.log(`Stake MoonCat #1000 via the Staking Portal`);
  await PORTAL.stakeToken(5054);
  console.log(`Stake MoonCat #5054 via the Staking Portal`);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
