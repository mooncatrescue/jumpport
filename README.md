# JumpPort Portals and Administration
The JumpPort contract allows for programmable interactions with ERC721 assets deposited into it. This repository is the development ground for several Portal contracts to interact with that ecosystem.

# Development
To run this application locally, the initial setup required is:

- Install Docker and prepare local `node:mooncat` image according to [this guide](https://gitlab.com/mooncatrescue/dev-environment) if this is your first MoonCatRescue project being worked on.
- Copy the `DOTENV_TEMPLATE` file to `.env`
- Edit `.env` and replace the `API_URL` value with your own URL to an Ethereum node (sign up for a free [Alchemy](https://alchemy.com/?r=6cccf5016cfcb3ed) or [Infura](https://www.infura.io/) account if you don't have one).
- Run `docker run -it --rm -u node -v ${PWD}:/app -w /app node:mooncat bash` to enter a Node environment for this project
- Run `npm install`

## Run Tests

```
npx hardhat test test/1_basic.js
```